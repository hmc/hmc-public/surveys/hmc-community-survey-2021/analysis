[![Project Status: Inactive – The project has reached a stable, usable state but is no longer being actively developed; support/maintenance will be provided as time allows.](https://www.repostatus.org/badges/latest/inactive.svg)](https://www.repostatus.org/#inactive)
=======
Code: [![Code License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) Data: [![Data License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

# Analysis of HMC Community Survey 2021

This project contains the questions included in the HMC Community Survey 2021 as well as the analysis scripts. The data has been published [here](https://data.gesis.org/sharing/#!Detail/10.7802/2433) or can be found in the [output](output/responses_cleaned_mapped_to_publish.csv) folder.

Data analysis was performed using [HIFIS-Surveyval](https://gitlab.hzdr.de/hifis/overall/surveys/hifis-surveyval) Framework. Custom analysis scripts that were written for preprocessing, data analysis and generation of plots are present in this project. We also added the scripts that were used to create the survey metadata (description of questions and data) in different formats.

In case of questions, please contact office@helmholtz-metadaten.de

## Authors
- [Witold Arndt](https://orcid.org/0000-0002-7713-9647)
- [Silke Christine Gerlich](https://orcid.org/0000-0003-3043-5657)
- [Volker Hofmann](https://orcid.org/0000-0002-5149-603X)
- [Markus Kubin](https://orcid.org/0000-0002-2209-9385)
- [Lucas Kulla](https://orcid.org/0000-0002-2484-2742)
- [Christine Lemster](https://orcid.org/0000-0001-7764-1517)
- [Oonagh Mannix](https://orcid.org/0000-0003-0575-2853)
- [Marco Nolden](https://orcid.org/0000-0001-9629-0564)
- [Katharina Rink](https://orcid.org/0000-0003-4874-3973)
- [Jan Schweikert](https://orcid.org/0000-0003-4774-2717)
- [Sangeetha Shankar](https://orcid.org/0000-0003-0387-7740)
- [Emanuel Söding](https://orcid.org/0000-0002-4467-642X)
- [Leon Steinmeier](https://orcid.org/0000-0001-9040-636X)
- [Wolfgang Suess](https://orcid.org/0000-0003-2785-7736)

## License

- Data is licensed under Creative Commons Attribution 4.0 International License: https://spdx.org/licenses/CC-BY-4.0.html
- Our scripts are licensed under GPL-3.0, see [LICENSE](./LICENSE): https://spdx.org/licenses/GPL-3.0-or-later.html
Copyright (c) 2022 Helmholtz Metadata Collaboration


## Project status
Completed.

## Acknowledgements

This project was supported by the Helmholtz Metadata Collaboration
(HMC), an incubator-platform of the Helmholtz Association within the framework of the
Information and Data Science strategic initiative.


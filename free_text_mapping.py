"""
Authors: HMC CCT1 TF Survey members
Last modified on 12.08.2022 
This file contains mapping for free-text answers from respondents
"""

# mapping: key = category name, value = list of free text answers from
# respondants

# v 1.0 by Sangeetha
# v 1.1 reviewed  by Markus
# v 1.2 reviewed  by Markus 09/02/2022
# v 1.3 reviewed by Sangeetha 14/03/2022
# question "Which international standards do you use?"
RDMPR6_other = {
    "toRemove": [0],
    "MIAME": [           # label: RDMPR6/6
        "Application specific standards (e.g. MIAMI)."
    ],
    "Others": [          # RDMPR6/other
        "GEO standard",
        "community standard formats fro data and metadata",
        "TDM"            # TODO double check: unkown significance; ShS: What does TDM stand for?
    ],
    "PubData": ["PubData"], #new category
    "CF Metadata Conventions": ["CF Conventions"], #new category
    "iFDO": ["iFDO"], #new category
    "BIDS": ["BIDS"], #new category
    "MIFlowCyt": ["MiFlowCyt"], #new category
    "DICOM": ["DICOM"], #new category
    "openPMD": ["openPMD"], #new category
    "PDBx/mmCIF": ["pdb-Koordinaten"], #new category
    "No standard": [  #new category
        "User generated",
        "XML, JSON", "QCDml"] # ShS: QCDml doesn't seem to be an international standard
}

# v_1.0 by Christine - 26/01/2022
# v 1.1 reviewed  by Markus 09/02/2022
# question: "Which Helmholtz center do you typically work in?"
PERBG1_other = {    
    "DZNE": ["DZNE"], # label: "PERBG1/DZNE"
    "HIM": ["Helmholtz Institut Mainz"],
    "DZIF": ["DZIF"]

}
# v_1.0 by Christine - 25/01/2022
# v_2.0 modified by Sangeetha - 14.04.2022
# reviewed by Lucas
# question: "Please select your principle research area."
PERBG3_other = {
    "Other": [
        "MTRA",
        "Professional Support",
        "Systemanalyse",
        "automatische Prozessierung von Erbeobachtungsdaten",
        "(Energie-)Wirtschaftswissenschaften",
        "vwl",
        "Social sciences", # omitted L1 "Social Sciences" in survey design.
        "Sociology, Technology Assessment", # omitted L1 "Social Sciences" in survey design.
        "Technology Assessment" # omitted L1 "Social Science" (best match "Humanities and Social Science: Social and Behavioural Science:Social Sciences:Safety Science").],
    ],
    "Physics:Particles, Nuclei and Fields": [  # no pre-defined label existing #Other?
        "Beschleunigerphysik"
    ],
    "Engineering Science:Computer Science": [  # label PERBG3ING/4
        "Computer science",
        "Computer Science and Medical Imaging",
        "Informatik",
        "Software",
        "Bibliothekswesen", # best match in SRAO ontology is "Knowledge and Information Systems", subcategory of "Engineering Science:Computer Science". Alternatively: Move to other?
        "Informationswissenschaften",
        "Data management",
        "Data Science", # moved here ("Engineering Science:Computer Science:Data Mining")
        "Datenwissenschaften" # moved here ("Engineering Science:Computer Science:Data Mining")]
    ],
    "Life Science:Biology": [  # label PERBG3LIFE/2
        "Biologie",
        "Biowissenschaften"],
    "Engineering Science:Materials Engineering": [ # label PERBG3ING/9
        "Material science",
        "Materials science",
        "Materialwissenschaften"],
    "Life Science:Medicine": [ # label PERBG3LIFE/5
        "Medizin",
        "Medizin/Gesundheit"],
    "Life Science:Medicine:Public Health": [ # label PERBG3MED/38
        "Gesundheitsforschung",
        "Gesundheitswissenschaften",
        "Health",
        "Gesundheitsprävention",
        "Epidemiologie",
        "Epidemiology",
        "Diabetes Epidemiology"],
    "Life Science:Biology:Bioinformatics": [ # label PERBG3BIO/2. (alternative match in SRAO ontology: "Engineering Science:Computer Science:Informatics:Bioinformatics"
        "Bioinformatics",
        "Informatik/Bioinformatik"],
    "Chemistry:Polymer Research": ["polymer"], # label PERBG3CHEM/12
    "Engineering Science:Systems Engineering": ["Robotik"], # label PERBG3ING/12
    "Life Science:Medicine:Medical Informatics": ["Medizininformatik"], # label PERBG3MED/17
    "Engineering Science:Energy Engineering": [ # label PERBG3ING/6
        "Energieforschung",
        "Energy system modeling"],
    "Earth Science:Environmental Science": ["Umwelt"], # label PERBG3GEO/2
    "Life Science:Biology:Ecology": ["Ecologie"], # label PERBG3BIO/12
    "Life Science:Biology:Biomedical Science": [ # label PERBG3BIO/3
        "neuroscience",
        "Biomedical Engineering"],
    "Life Science:Medicine:Medical Virology": [ # label PERBG3MED/20
        "Virology",
        "Infections, e.g. Covid-19"],
    "Engineering Science:Civil Engineering": [ # in SRAO ont. under "Engineering Science: Civil Engineering:Construction Engineering:Transportation Planning"
        "Transportation",
        "Verkehrswissenschaften"],
    "Physics:Meteorology": ["Meteorologie"] # label PERBG3PHYS/9
    }
    
# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3ING_other = {
    "Computer science": ["Softwareentwicklung"],
    "Transportation": [
        "Navigation",
        "Fahrzeugtechnik",
        "Verkehr",
        "Verkehrswesen",
        "Logistik/Verkehr",
        "Verkehrssystemtechnik",
        "Verkehrsingenieurswesen"],
    "Electrical Engineering": [
        "Photovoltaik",
        "Batteriematerialien"],
    "Environmental science": ["Umweltwissenschaften"],
    "System Engineering": ["Robotik"],
    "Martime Engineering": ["Sensortechnik im maritimen Bereich"],
    "Neuroscience": ["Neuroscience"],
    "Process Engineering": [
        "Chemieingenieur",
        "Materialwissenschaften"]}

# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3GEO_other = {
    "Geology": [
        "Geochemie",
        "Geoanalytik"],
    "Transportation": ["Verkehr"],
    "Water Research": ["Hydrologie"],
    "Geophysics": [
        "Geodäsie",
        "Remote Sensing",
        "Fernerkundung",
        "Erdbeobachtung",
        "Planetenforschung"],
    "Geography": ["Urban"],
    "Atmospheric Science": ["Klimatologie"],
    "Data Science": ["Daten"]}

# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3MATH_other = {
    "Data Science": ["Data Science"]
}

# v_1.0 by Christine - 26/01/2022
# reviewed by Lucas
PERBG3PHYS_other = {
    "Interdisciplinary": [
        "Multiple areas crossing over physics, biology and material science"
    ],
    "Electrical Engineering": [
        "Roentgendetektoren"],
    "Biophysics": [
        "Medical Physics",
        "Medical",
        "Medizinische Physik"],
    "Oncology": ["Onkologie"],
    "Atmospheric Science": [
        "Atmosphere",
        "space physics",
        "Space Weather"],
    "Atomic, Molecular, Optical and Plasma Physics": [
        "Optik",
        "Relativistische Laser Plasma Physik",
        "Röntgenoptik",
        "X-ray Imaging"],
    "Accelerator Physics": [
        "Accelerator physics",
        "Beschleuniger",
        "Beschleunigerphysik",
        "Beschleunigerphysik und -technologie"],
    "Particles, Nuclei and Fields": ["Angewandte Kernphysik"],
    "Theoretical Physics": ["theoretical physics"],
    "Quantum information": [
        "Quanteninformation",
        "Quantum Information"],
    "Materials Science": [
        "Materialanalyse",
        "Solid state"]}

# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3LIFE_other = {
    "Microbiology": ["Microbiology"],
    "Biomedical Science": ["Immunology", "Neuroscience"],
    "Radiobiology": ["Strahlenforschung"],
    "Transportation": ["Verkehrsforschung"],
    "Biotechnology": ["Biotechnology + Automation"],
    "Biochemistry": ["Biochemie"]
}

# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3BIO_other = {
    "Omics": ["Genomik"],
    "Biomedical Science": ["Immunology", "Neurobiologie"]
}

# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3MED_other = {
    "Public Health": ["Epidemiologie"]
}

# v_1.0 Christine 26/01/2022
# reviewed by Lucas
PERBG3PSYCH_other = {
    "Biological Psychology": ["Kognitive Neurowissenschaften"],
    "Engineering Psychology": [
        "Engineering Psychology / Human Factors",
        "Human Factors",
        "Ingenieurspsychologie",
        "Ingenieurpsychologie"],
    "Transportation": ["Verkehrspsychologie"],
    "Cognitive Psychology": ["Kognitive Psychologie"],
    "Experimental Psychology": ["Experimentelle Psychologie"]}

# v_1.0 Christine 25/01/2022
# reviewed by Lucas
PERBG3CHEM_other = {
    "Physical Chemistry": [
        "Electrochemistry",
        "Radiochemie",
        "Radiochemistry",
        "Radiation chemistry",
        "Kernchemie"],
    "Material Science": [
        "Material chemistry",
        "Materialwissenschaft"]}

PERBG3AGRI_other = {}

# version 1.0 by Leon 25/01/2022
# v 1.1 reviewed by Markus 10/02/2022
# v 1.2 reviewed by Markus 28/02/2022
# v 1.3 reviewed by Sangeetha 14/03/2022
# Q: Which is your current career level?
PERBG6_other = {
    "toRemove": [0],
    "Research Associate": [  # label "researchAssociate"
        "scientist",
        "Wissenschaftlerin",
        "Senior Scientist",
        "Scenior scientist",
        "staff scientist",
        "researcher"
    ],
    "Postdoc": [  # label "postdoc"
        "Befristeter Projetkleiter*in aka Postdoc"  # moved here
    ],
    "Principal Investigator": [  # label "principalInvestigator"
        "Abteilungsleiter",  # department leader
        "Abteilungsleitung",
        "Abteilungsleiter*in",
        "Department leader",
        "Head of Department",
        "Branch head",
        "Gruppenleiterin",  # group leader
        "Gruppenleiter*in",
        "Gruppenleiter",
        "Gruppenleitung",
        "Teamleiter",
        "Arbeitsgruppenleiter",
        "Arbeitsgruppenleitung",
        "Forschungsgruppenleiter",
        "Nachwuchsgruppenleiter",
        "Associate professor, Dr"
    ],
    "Other": [  # label: PERBG6/other
        "Lab/Project Manager",
        "Dokumentar",
        "Data Librarian",
        "Data manager",
        "Programme Manager",  # other
        "staff"
    ],
    "Technical Staff": [  # new answer category (to be included in future surveys)
        "technischer Angestellter",
        "Technikerin",
        "Labortechniker",
        "technische Assistenz",
        "Techn. Mitarbeiter",
        "technische Assistentin",
        "Technische Assistentin",
        "Technician",
        "Technischer Mitarbeiter",
        "technisch/wissenschaftlicher Mitarbeiter",
        "MTRA"
    ],
    "Management and Coordination": [  # new answer category (to be included in future surveys)
        "Administrative Leiter*in",  # moved here
        "Research facility manager",  # moved here
        "Study Coordinator",
        "Projektmanager*in",  # project manager
        "Projektkoordination",
        "Project Manager, MBA",
        "coordinator",
        "Koordinatoin",
        "Scientific coordinator",
        "scientific coordinator",
        "administrativ tätig",  # moved here
        "Organisation",  # moved here
        "IT-Manager",  # moved here
        "IT-Management"  # moved here
    ]
}
# version 1.0 by Markus 28/01/2022
# v 1.1 reviewed by Markus 10/02/2022
# Q: Large scale facilites (other)
RSDP1c_other = {
    "toRemove": [0],
    "DESY": [  # label of existing answer #TODO DESY doesn't exist in the original answer options. New category?
        "DESY",
        "DESY-II, SPS"
    ],
    "Synlight": [  # "Synlight (DLR)" to be added in future surveys
        "Synlight"
    ],
    "IBC": [  # "Ion Beam Center (HZDR)" to be added in future surveys
        "IBC"
    ],
    "bERLinPro": [  # "bERLinPro (HZB)" to be added in future surveys
        "bERLinPro"
    ],
    "PITZ": [  # "PITZ (DESY)" to be added in future surveys
        "PITZ"
    ],
    "External Synchrotron facilities": [ #new category
        "ESRF",
        "Elettra",
        "Diamond Light Source",
        "ALS, Diamond, SPring-8, Elettra",
        "PSI, ESRF",
        "ESRF, SLS",
        "SLS",
        "ESRF, GKSS (HZG)",
        "Synchrotrons",
        "MAX-lab (before MAX IV)",
        "ESRF, Diamond Light Source",
        "LCLS/APS/ESRF",
        "LCLS (SLAC)",
        "ILL, ESRF. PSI",
        "ESRF, LCLS/SLAC, Fermi, DLS, MaxLab",
        "MLZ, ILL, ESRF, SLS, ANSTO",
        "LCLS, ALS, ESRF",
        "NSLS"],
    "External neutron sources": [ #new category
        "ILL",
        "ILL, SINQ, MLZ",
        "ILL, MLZ, ORNL",
        "MLZ, ILL, SNS",
        "SINQ",
        "MLZ",
        "FRM II",
        "FRM2",
        "HLD (HZDR), ILL"],
    "Plasma facilities": [ #new category
        "W7-X, ASDEX Upgrade"
    ],
    "Computing facilities": [ #new category
        "HPC Cluster, M2VP,SRV",
        "HPC Centers"
    ],
    "Astronomical facilities": [ #new category
        "VERITAS, CTA, XMM-Newton, Fermi LAT, other astronomical facilities"
    ],
    "External Accelerator Facilities": [ #new category
        "DAPHNE (IT), AGOR (NL)",  # interpreted as DAFNE (Rome, IT)
        "SuperKEKB"]
}

# v 1.1 reviewed by Markus 10/02/2022
# reviewed by Sangeetha 15/03/2022
# Q: Please select the data formats that you generate or use in your current
# research project.
RSDP3_other = {
    "Image formats": [  # label "RSDP3/5" # (e.g., JPEG, JPEG2000, GIF, TIF, PNG, SVG)
        "Vector graphics figures in SVG, PDF or EPS (analysis results, not raw data)"
    ],
    "Plain text": [  # label "RSDP3/7" # (TXT in several encodings)
        ".xy",
        "gene sequences, FASTA",
        "mmCIF",
        "NIfTI",
        "NIFTI"
    ],
    "Application-specific formats": [  # label "RSDP3/10" # (e.g., from modelling tools, IDEs, compilers)
        "ibw - Igor Binar",
        "Georeferenzierte Vektordaten (ESRI Shapefiles), GeoPackage (GPKG)", 
        "RData, Raster(.grd), Shapefiles (.shp) "
    ],
    "Structured text": [  # label "RSDP3/14" #  (e.g., JSON, XML, SGML, CSV, BLAST)
        "Geodatan (Shapefiles, GeoJSON, KML)", 
        "sematische Datenmodell (IFC, CityGML, GeoJSON, GML etc.",
        "spatial data like shp, geotiff"
    ],
    "Binary scientific formats": [  # label "RSDP3/15" # (e.g., HDF5, netCDF, CBF)
        "HDF5",
        "NetCDF",
        "Sequencing formats (fast5, fastq)",
        "FASTQ, BCL",
        "Genomics specific binary formats (BAM, CRAM)",
        "fcs",  # Flow Cytometry Standard (FCS) format (binary)
        "CGNS, numpy NPY format"  # TODO: double check assignemnt
    ],
    "Other": [  # label "RSDP3/other"
        "RData, Raster(.grd), Shapefiles (.shp)",  # TODO double check
        "Eigenes hocheffizientes Format",
        "Gedruckt aus einem System, gescannt als Bild ins andere System, dann abgetippt oder per Programm ausgelesen",
        "Blickdaten als Fixationen mit Dauer und Koordinate",
        "CDF, IRIG Chapter 10",
        "department-specific format",
        "nid, gwy",  # TODO please check whether to assign elsewhere
        "PDF",
        "pdf",
        "miniSEED",  # TODO please check whether to assign elsewhere
        "CPACS",  # TODO please check whether to assign elsewhere
        "zmx",  # TODO please check whether to assign elsewhere
        "TDMS data",  # TODO please check whether to assign elsewhere
        "MSSQL",  # TODO please check whether to assign elsewhere
        "UML Diagramme",
        "OVF",  # TODO please check whether to assign elsewhere
        "ROOT-files",  # TODO please check whether to assign elsewhere
        "ROOT",  # TODO please check whether to assign elsewhere
        "Root",  # TODO please check whether to assign elsewhere
        "ROOT; Python-PANDA Dataframes",  # TODO please check whether to assign elsewhere
        "ROOT and Python data structures",  # TODO please check whether to assign elsewhere
        ".ROOT und .TSA",  # TODO please check whether to assign elsewhere
        "Listmode Daten"  # TODO please check whether to assign elsewhere
    ],
    "Binary non-scientific formats": [  # new answer category: add in future surveys
        "EIgene Binärfiles",
        "Binäre proprietäre Formate",
        "Binärdaten ohne inlkudierten Metadaten",
        "eigene Binärformate",
        "Binärdaten raw",
        "binary community specific and ILDG format"
    ],
    "toRemove": ["bla bal"]
}

# v 1.1 reviewed by Markus 10/02/2022
# v 1.2 reviewed by Sangeetha 15/03/2022
# Q: Which of the following motivated you to publish your data? (Please
# choose up to 3 options)
DTPUB3_other = {
    "Reusability": [  # label DTPUB3/1
        "So others can use it for simulation",
        "Daten der Community für aufbauende Forschungsarbeiten zugänglich machen"
    ],
    "Guidelines/policies of institute/funder": [  # label DTPUB3/4
        "My boss told me to",
        "Vorgabe des Doktorvaters und persönlicher Vorteil (Doktorgrad)"
    ],
    "Good scientific practice": [  # label DTPUB3/5
        "Nachprüfbarkeit der Ergebnisse",
        "to attest to the veracity of my research",
        " Förderung open source data",  # TODO double check / remove leading space?
        "To align with open source principles"  # TODO double check
    ],
    "Other": [  # label DTPUB3/other
        "I upload others dara",
        "Anschaulichkeit"
    ],
    "Guidelines / policies of publishers": [  # new category: to be added in future surveys
        "Die Konferenz hat es verlangt, dass Reproduzierbarkeit sichergestellt ist",
        "To take part in conferences",
        "Forced by Journal rules",
        "Review / Korrektur meiner Rechnungen",
        "Vorgabe des Journals",
        "Das Journal hat es verlangt.",
        "vorgabe des journals",
        "Vom Journal gefordert",
        "Anforderungen des Journals",
        "Reviewer hat darum gebeten",
        "Vorgaben von  wissenschaftlicher Zeitschriften",
        "Anforderungen der Zeitschriften; Förderung open source data",
        "Bedingung des Journals zur Submission",
        "die Richtlinien des Journals verlangten es",
        "Pflicht zur Veröffentlichung in manchen Journalen",
        "Vorgaben der Journale",
        "required by journal"
    ],
    "toRemove": [
        "Bla Bl a",
        "niemand"
    ],
    "I have not yet published data": [  # no label: opt-out
        "I don‘t publish the data",
        "Try to avoid to publish the data",
        "Noch nicht geschehen.",
        "not yet published",
        "habe ich noch nicht genutzt (würde aber gern)"
    ]
}

# preliminary assigments by MK 2022/02/15
# v 1.2 reviewed by Sangeetha 15/03/2022
# Q: How did you publish your data?
DTPUB1b_other = {
    "toRemove": [0, "Bla Bl"],
    "Supplementary to journal publication": [  # label: DTPUB1b/1
        "Supplemental Material beim Artikel direkt",
        "We included a code with which the data were generated",
        "Anhang"
    ],
    "In repository": [  # label: DTPUB1b/2
        "öffentlich zugängliche Datenbanken",
        "Rodare",
        "CERA Datenbank am DKRZ",
        "NASA und ESA Archive (PDS und PSA)",
        "geonetwork implementation des zentrums",
        "In der dafür vorgesehenen Datenbank der ESA",
        "as DOI from a facility",
        "WebGIS",
        "EOC Geoservice",
        "ROBIS",
        "geoservice",
        "Datenservice des DLR",
        "Zenodo"
    ],
    "Other": [  # label: DTPUB1b/other
        "Nur wesentliche Ergebnisse alsGrafik und einzelne Zahlenwerte.",
        "Ausgewertete Daten als Graph, Bild oder Video",
        "Website",
                "I make near realtime data publicly accessible.",
                "Webseite",
                "In most cases, only selected data-sets that make it into publications are actually published. Datasets recorded during the optimization of the apparatus, during preparation of the final experiments etc. are usually not published. However, such data is shared within the group.",
                "own publication hub",
                "Sobald die Arbeit abgeschlossen ist als API der Plattform, die ich entwickle (da es sich um wöchentlich erweiternde Daten handelt, macht es keinen Sinn sie als statisches Objekt zu veröffentlichen)",
                "via website",
                "ftp",
                "Datenserver + Github + Webseitendokumentation",
                "eigens eingerichtete Website"
    ],
    "No data published": [  # no label: opt-out
        "Daten werden auf Anfrage zur Verfügung gestellt",  # data is provided upon request
        "auf Anfrage zur Verfügung gestellt",
        "auf Nachfrage über die Autoren",
        "to be sent upon reasonable request",
        "\"on request\"",
        "datenweitergabe bei Anfrage",
        "In Form von Graphen/Grafiken in Fachpublikationen",  # figures / tables in paper
        "als Fachartikel",
        "plots",
        "Normales Journal, die Datenmengen werden entsprechend grafisch aufbereitet",
        "plots und tabellen im paper",
        "excerpts in publications (tables, graphs, pictures, videos)",
        "Es werden nur ausgewertete Simulationen veröffentlicht, nicht die Simulationsdaten selber",
        "Nur im Paper",
        "I only publish the summarised results",
        "nur erzeugte Bilder in Publikationen",
        "Konferenz",  # presentation (?) in a conference
        "Kongress",
        "Konferenzen",
        "Konferenzbeitrag",
        "intern im Institut",  # institutional archival (no publication)
        "Intern",
        "Datensätze auf lokalem Speicher gesichert",  # local archival
        "Bisher keine veröffebtlichung von erhobenen Daten",  # no data published
        "z.B. Bilder vom Strahlprofil",
        "Bisher gar nicht",
        "Upon request",
        "not yet published",
        "not yet published but intention to do so.",
        "weiß nicht wie genau",
        "noch gar nicht",
        "keine publikation bisher",
        "Bisher noch nicht.",
        "I am not allowed to publish data",  # not allowed to publish data
        "I am limited when using large epidemiological cohorts with data privacy rules. We specify that data can be made available upon request but for the publication itself I may only publish code.",
        "Preliminary work for scientific evaluation",  # unclear
        "Hinweise zu Quellen der Anbieter",  # reused data
        "Open source data treatment code"  # source coude publication
    ]
}

# Done by Lucas Kulla
# v 1.1 reviewed by Markus 14/02/2022
# v 1.2 reviewed by Sangeetha 15/03/2022
DTPUB4a_other = {
    "toRemove": ["blavla"],
    "No obstacles":["Bisher noch keine."], # label: DTPUB4a/0
    "Lack of time / personnel": [  # label: DTPUB4a/2
        "Die Erstellung von korrekten Metadaten ist zeitaufwendig.",  # moved here
        "Lack of trained personell",  # moved here
        "Lack of skilled personell",  # moved here
        # moved here: time-consuming -> lack of time
        "Aufebereitung der Daten für allgemeinen Publikumsverkehr ist sehr aufwändig",
        "Sehr zeitaufwendig, vor allem von der Kosten-Nutzen Seite (wenig Impact für viel Arbeit, Nachnutzung noch unklar, Submission bei Scientific Data abgelehnt)"
    ],
    "Lack of incentives": [  # label: DTPUB4a/3
        "Rohdaten veröffentlichen scheint im Bereich nicht gängige Praxis zu sein"  # moved here
    ],
    "Technical barriers": [  # label: DTPUB4a/5
        "Lack of general purpose repositories for mass spectral data",
        # moved here from "no workflow"
        "Wir müssen uns auf ein einheitliches Vorgehen einigen, damit unterschiedliche oder gleiche Daten möglichst nicht über verschiedenen Wege (Repositorien) geteilt werden."
    ],
    "Legal / ethical concerns": [  # label: DTPUB4a/6
        "Klärung lizenzrechtlicher Fragen im Einzelfall umfangreich",  # moved here
        "Politische/rechtliche Voraussetzung für die Veröffentlichung von Daten fehlen",  # moved here
        "Urheberrecht",  # moved here
        "The Guidelines of the Collaboration I work within are very restrictive.",  # moved here
        "Data stems from collbaorations including industry: secrecy",  # moved here
        "Organisatorische Hinternisse, u.a. Datenfreigabe, Exportkontrolle",  # moved here
        "Exportkontrolle",  # moved here
        "The data provider/project partner doe not want the data to be published"  # moved here
    ],
    "Technical support needed": [  # label: DTPUB4a/7
        "Bedenken im Institutsmanagement",  # moved here
        "Freigabe der Daten durch meine Einrichtung"  # moved here
    ],
    "Other": [  # label: DTPUB4a/other
        "keine DOI",
        "Oft ist mir nicht klar, welche meiner Daten irgendjemande nutzen koennen, und scheue den Aufwand dann"
    ],
    "Insufficiently trained": [  # short label, e.g.: "lack of training" # add in future surveys
        # toDO: double check - might be moved to DTPUB4a/7
        "I don't have enough data-management skills",
        "Es mangelt an Wissen, wie Forschungsdaten sinnvoll zur Nachnutzung aufbereitet werden können"
    ],
    "No value to others": [  # short label, e.g.: "no value" # add in future surveys
        "Viele Daten werden bei Vorarbeiten erzeugt, die nicht veröffentlichungswürdig sind."
    ],
    "Competition": [  # short label, e.g.: "competition" # add in future surveys
        "concerns regarding losing know-how advantage",
        "eigene Position wird durch Konkurrenz geschwächt, wenn diese die Daten nicht zitiert und alleine für neue Ergebnisse nutzt",
        "Students and collaborators are reluctant to open their datasets and source code"
    ],
    "No data published": [  # short label, e.g.: "not published data"
        "not yet published",
        "i have not yet published data (apart from code)"
    ]
}

# Done by Lucas Kulla
# reviewed by MK 2022/02/16
# v 1.1 reviewed by Sangeetha 15/03/2022
# Q: What concerns or obstacles have discouraged you from publishing 
# your research data so far?
DTPUB4b_other = {
    "Technical support needed": [  # label: DTPUB4b/7
        "FAIR ist eine Kopfgeburt die so wie sie betrieben wird zum Scheitern verurteilt ist. Es gibt keinerlei Unterstützung und Gedanken eine sinnvolle und nutzbare Datenerhebung umzusetzen. Dazu gehören Softwaretools zur Verwaltung von Proben, die Aufnahme von Metadaten von seitens des Großgerätes, eine sinnvolle und gut nutzbare Laborbuchanwendung sowie die Implementierung der Kommunikation der verschiedenen Softwarekomponenten untereinander."
    ],
    "Insufficiently trained": [  # short label, e.g.: "lack of training" # TODO: add in future surveys
        "Lack of knowledge on how to do it in practice",
        "Mir fehlt ein Konzept, in welcher Form Daten veröffentlicht werden können, z.B. Experiment-basiert oder Objekt-basiert?"
    ],
    "Lack of incentives": [  # label: DTPUB4b/3
        "zu viel Arbeitsaufwandd/zu wenig Nutzen,",
        "Die Journals, in denen wir publizieren, sehen meist die Veröff. von Rohdaten nicht vor"
    ],
    "Legal / ethical concerns": [  # label: DTPUB4b/6
        "Daten fallen unter die Export Kontrolle.",
        "DLR Regularien verhindern es",
        "Exportkontrolle / Dual Use",
        "Ergebnisse beinhalten teils Daten, die nicht veröffentlicht werden dürfen.",
        "Lizenz an Projekt/Forschungsarbeit gekoppelt",
        "Data is for the whole collaboration, not just me.",
        "Die Forschungsdaten göhren mir eigentlich nicht, sondern einer großen Kollaboration",
        "es sind nicht meine Daten"  # moved here
    ],
    "Competition": [  # see also DTPUB4a, short label, e.g.: "competition"
        "Die Daten bleiben für uns wissenschaftlich wertvoll. Im Lauf der Jahre ergeben sich neue Forschungsfragen, die auch mit älteren Datensätzen noch beantwortet werden können."
    ],
    "No value to others": [  # short label, e.g.: "no value" # add in future surveys
        "Experiments are quite special and datasets are useless without corresponding labbook entires/metadata. It would be extremly time consuming and challenging to document all these things in a way that someone not involved in the experiment could understand the data",
        "Daten selbst sind nicht relevant",
        "Es ist für mich unklar, ob man sich für solche Daten interessiert (es kostet Zeit, um scheinbar, niemand in unsere Bereicht macht es im Moment)",
        "Ich bezweifle, dass jemand diese Daten nutzen würde",
        "Ich bezweifle, dass meine sehr speziellen Daten für Dritte von Interesse sein könnten.",
        "kein Interesse anderer",
        "Die Daten können nicht von anderen ohne weiteres verarbeitet werden.",
        "Die mikroskopischen Bilder sind bereits in den Abbildungen der Publikation in ausreichender Auflösung verfügbar.",
        "no decision made on publishing the data and also the research data was not considered relevant enough for publishing earlier",
        "Datensatz ist leicht wieder zu generieren",
        "It's available upon request, but no one requests it"
    ],
    "Other": [  # label: DTPUB4b/other
        "Daten dienen nur bisher nur zur Veranschaulichung der Theorie",
        "Wir veröffentlichen verarbeitete Daten, keine Messdaten",
        "Never tried",
        "data is available through project agreements (https://helmholtz-muenchen.managed-otrs.com) subject to approval by the study board",
        "not yet implemented",
        "Software/Algorithmen sind der primaere Output und nicht Daten"
    ],
    "toRemove": [  # no reasons for not publishing data given:
        "Daten liegen in Repositorium und sind auf Antrag nutzbar",
        "Ich arbeite gerade an der Publizierung der Daten",
        "Zuarbeit für WissenschaftlerInnen"
    ]
}
    
# version 1.0 by Markus 28/01/2022
# reviewed by Christine 15/02/2022
# v 1.1 reviewed by Sangeetha 15/03/2022
# Q: Where do you store / archive your data?
RDMPR1_other = {
    "toRemove": [
        0,
        "bla",
        "Not finished yet"],
    "Locally": [  # label: RDMPR1/1 #(e.g., in a computer, on an external data storage medium, in the instrument or device)
        "privat: dezentral",
        "Innerhalb der Kollaboration"],
    "Externally (servers / repositories)": [  # label: RDMPR1/2 
        "privater Cloudspeicher",
        "Sciebo"],
    "Centrally (internal server)": [  # label: RDMPR1/2 - TODO in future surveys: add ".. or archive" # on an internal server (e.g., working group, institute/department, data center)
        "Magnetband-Archiv",
        "tape archive",
        "interner Langzeitarchiv",
        "Archivfestplatten (Gute wissenschaftliche Praxis)",
        "DKRZ, LRZ",
        "EOC DIMS"],
    "Other": [  # label: RDMPR1/other
        "Das ist je nach Projekt unterschiedlich."],
    "Most data is lost": [  # new answer category (add in future surveys?)
        "Most research data is lost after the project end due to HPC storage being coupled with the project, i.e. at the end of the project the storage is no longer available.",
        "gar nicht"]
}

# first version: ?
# review: MK 2022/02/17
# v1.2 reviewed by Sangeetha 15/03/2022
# Q: In your current project, where do you document the steps used to
# generate and process your data?
RDMPR3_other = {
    "toRemove": [
        "b",
        "No data generated yet",
        "Selbst wenn ich digtiale Textdokkumente angekreuzt habe, ist die Qualität äquivalent zu \"es erfolgt keine Dokumentation\"",
        "bla"],
    "Digital system": [  # RDMPR3/2 # (e.g., Electronic laboratory notebook (ELN), Laboratory Management System (LIMS), Wiki)
        "workflow management system",
        "sample database",
        "e-logbook",  # moved here
        "Internes Wiki",  # moved here
        "In the metadata system",  # moved here
        "Confluence"  # moved here
    ],
    "Digital text": [  # RDMPR3/3 # (e.g., Word, google-docs document, readme file, PDF form, XML File)
        "Zusammen mit den Daten im Repositorium",
        "Markdown im Code",  # moved here
        "Log-Dateien von Software",  # moved here
        "Log files",
        "OneNote",  # moved here
        "Mails (which are stored internally)"  # moved here
    ],
    "Other": [  # RDMPR3/other
        "Publikationen und wissenschaftliche Arbeiten",  # moved here
        "Die Bearbeitung ist einigermaßen in die Veröffentlichungen beschrieben",  # moved here
        "verschieden",
        "Sichern der Software inclusive der verwendeten Namelists und Konfiguration"
    ],
    "GitLab": [  # new category (add in future surveys?) #TODO: comes under digital system?
        "Gitlab",
        "project documentation on github, gitlab",
        "Versionskontrolle durch Git mit ausführlichen Commit-Messages",
        "git",
        "Revision history (Git-based)",
        "Wiki",
        "Git",
        "GIT",
        "In Markdown direkt in einem Projekt Git Repository.",
        "Versionierungssoftware: Git, GitLab, ELO",
        "Versionskontrolle-Plattformen (GitHub, GitLab)",
        "Quellcode in Versionsverwaltungssystem"
    ],
    "Source code and scripts": [  # new category (add in future surveys?) #TODO: comes under digital text?
        "Im Skript selbst",
        "Skripte zur Automatisierung von Simulationen",
        "scripts for automatically running the whole simulatioj workflow",
        "Im Programmskript",
        "SQL-Skript, SAS-Programm",
        "scripts",
        "Scripts that automate the data generation",
        "R-Skripte",
        "Source code comments",
        "Python scripts",
        "Within the source code",
        "Teilweise Dokumentiert im Quellcode",
        "Auswertescripte",
        "Dokumentation im Quell-Code (in Python Skripte)",
        "R-Codes",
        "Setup files für Rechenfälle+eigene Tutorials im Team gepflegt, gesichert, z.T. publiziert",
        "Standardisierte Workflows mit beigefügtem Quellcode",
        "Jupyter Notebook",
        "Jupyter Notebooks",
        "SOPs, Jupyter Notebooks"  # moved here
    ]
}

# first version: ?
# reviewed by MK 16/02/2022
# v 1.2 reviewed by Sangeetha 15/03/2022
# Q: Please select which information (metadata) you typically use to 
# describe your research data.
RDMPR7_other = {
    "toRemove": [
        "?",
        "Unklar",
        "bla"],
    "Information on data collection": [  # label: RDMPR7/3 # (e.g., devices and parameters used, workflows, and working steps, software)
        "Andere Parameter der Maschine und des Experiments",  # moved here
        "various supplimental information  about measurement conditions"
    ],
    "Data processing and analysis": [  # label: RDMPR7/5 # (z.B. verwendete Software, Analyseskripte, Arbeitsschritte)
        "\"Parameters used\" is important also for analysis (processing scripts), as well as the experiment"
    ],
    "Description of data structure": [  # RDMPR7/6 #(e.g., meaning of variables in spreadsheets, data formats)
        "Dateistruktur + Name"
    ],
    "Others": [  # RDMPR7/other
        "möglichst mit Standard-Metadaten"
    ],
    "Instructions for reuse": [  # maybe for reuse in future surveys
        "Installationsanweisung"
    ]
}

# first version: ?
# reviewed by MK 16/02/2022
# v 1.1 reviewed by Sangeetha 15/03/2022
# Q: Which of these metadata do you publish along with your research data?
DTPUB7_other = {
    "Logs / statistics": [  # label: DTPUB7/92
        "Validation"  # moved here
    ],
    "Others": [  # label: DTPUB7/other
        "unterschiedlich",  # moved here
        "Beispieldaten für einzelne Messungen",  # moved here
        "was notwendig erscheint",  # moved here
        "sie sind Grundlage vonn wissenschaftlichen Publikationsdaten",  # moved here
        "Supplementary material"  # moved here
    ],
    "Scope of data set": [  # new category (add in future surveys?)
        "Purpose of acquisition"
    ]
}

# v_1.0 by Christine - 26/01/2022
# reviewed by Lucas
# v 1.1 reviewed by Sangeetha 15/03/2020
RDMPR12_other = {
    "Administrative guidelines": ["Existing internal structures"], #RDMPR12/6
    "Provide research data context": [ #RDMPR12/2 # Research data can be put into context more easily
        "Nachvollziehbarkeit durch Kollegen",
        "Ease of comparison"
    ],
    "Improved collaborations": [ #new category
        "Leichtere Zusammenarbeit im Team",
        "erleichterte Zusammenarbeit"
    ],
    "Improved working with data": [ #new category
        "Datenvisualisierung mehrerer Datensätze ist so leichter",
        "Interoperability",
        "Ermöglicht eine (tei-)automatisierte Datenanalyse",
        "Vereinfachtes Arbeiten mit den Daten",
        "Daten können auch in Zukunft genutzt werden"
    ],
    "Use of Standard": ["EIheitlicher Standart"] #new category
}

# v_1.0 by Christine - 26/01/2022
# reviewed by Lucas
# reviewed by MK 2022/03/01
# v 1.1 reviewed by Sangeetha 15/03/2020
# Q: What obstacles or difficulties have you encountered in collecting metadata 
# as part of your work?
RDMPR11_other = {
    "Other": [  # label RDMPR11/other
        "daten nützen nichst, wenn sie nicht ausgewertet werden",
        "We are at the very beginning of implementation"
    ],
    "toRemove": ["bal"],
    "Lack of technical knowledge": [  # label: RDMPR11/5
        "Fehlende Erfahrung zu vorhandenen Lösungen"
    ],
    "Lack of resources": [  # label: RDMPR11/1 # (e.g., time, personnel, financial resources)
        "Die Nutzbarmachung der zur Verfügung stehenden Infrastruktur nimmt zu Beginn viel Zeit in Anspruch",
        "Aufwand/fehlende wiederverwertbarkeit von (selbst erstellten) Templates",
        "It is very time-consuming to research which ontologies and ontology concepts are best to use.",
        "It's time-onsuming"
    ],
    "Relevant metadata is missing": [  # new category (add in future surveys?)
        "Mangelhafte/keine Dokumnentation von Metadaten zu älteren eingebundenen Datensätzen",
        "historische Daten haben oft keine ausreichenden Metadaten",
        "backtrack metadata of legacy data is difficult",
        "Different coding/tagging e.g. SubjectID, ID, SampleID...",
        "Bei Flugmessungen ist die gesamte Messkette nicht immer klar. D.h. ich weiß nicht immer, wie die Sensoren wirklich funktionieren und brauche daher Abscgätzmethoden, um die Richtigkeit der Messungen beurteinen zu konnen.",  # moved here
        "Collaborators are reluctant to share details due to perceived issues with competition"  # moved here
    ],
    "Commercial applications complicate standardized solutions": [  # new category (add in future surveys?)
        "Anlagen, die gekauft sind, lassen sie sich nicht ändern, damit die Metadaten Aufnahme besser strukturiert sein kann"
    ],
    "Lack of technical solutions": [  # label: RDMPR11/6
        "Poor technical solutions for collecting in a structured way",
        "Metadaten sind zwar vorhanden, aber nur mittels SQL-Abfragen mit den Rohdaten zu verbinden. Besser wäre eine durchgehende Nutzung von Formaten die Metadaten mit Rohdaten verbinden, bspw DICOM",
        "lack of standards",
        "fehlendes einheitliches Format",
        "Es ist unklar welche wichtig sind.",
        "lack of international standards and lack of resources for harmonisation",
        "no templates available",
        "fehlende einheitliche Lösungen",
        "unclear conventions for naming metadata.",
        "Fehlende zentrale Lösungen",
        "Fehlende best practices, da eigentlich genug technische Lösungen existieren müssten",
        "Fehlende Unterstützung von der experimentellen IT"
    ],
    "Heterogeneous workflows complicate standardized solutions": [  # new category (add in future surveys?)
        "Sehr heterogene Date machen Standardisierung schwierig, wodurch der Nutzen, den man hätte, stark eingeschränkt wird",
        "forschungsdaten ind zu variabel für formalisierte erfassung",
        "for very customized experiments it is challenging to collect correct metadata sets in a structured way",
        "jeder macht sein eigenes System und nciht alle Kollegen arbeiten mit Struktur",
        "Not really possible to automate full experiment (some things are switched or triggered by hand, e.g. to record the spectrum or power of the laser pulse which will be used for experiment). Would still be a lot of work to create a metadata collection system able to communicate with 5-10 kinds of instruments, so only the most important (changing often, self-made software so communication possible to add) has been automated at the moment."
    ]
}

# v_1.0 Christine - 25/01/2022
# reviewed by Lucas
# reviewed by Markus
# v 1.1 reviewed by Sangeetha 15/03/2022
SERVC1_other = {
    "toRemove": [0, "bla","überall!", "Es gibt unterstützende Angebote?", "mehr ZEIT zu Verfügung dazu, auch Automatisierung ist nötig"],
    "RDM software & tools": [  # label: SERVC1/5
        "Unterstützung bei der Automatisierung der Datenaufnahme in Datenbanken und Langzeitarchivierung",
        "Angebote an gemanagte Datenbanken, die über MS SQL hinausgehen (z.B. neo4j o. JanusGraph, Datomic, MongoDB, …) und ein gemanagter Kubernetes-Cluster"
    ],
    "Financial resources for RDM": [  # new answer category
        "Personal",
        "With unlimited money, we could have some people doing this full time or if I had a permanent employment as technician instead of aiming for publications there would be time to build up such systems. Our research group is 15 people including PhD students, and the experiments are not much using big-facility hardware or software that anyone else could take care of without our domain expertise",
        "Personal and Time. Who is supposed to do all that?"
    ],
    "Technical aspects of RDM": [  # label: SERVC1/9
        "Bereitstellung von Speicherplatz durch eigenes Institut",
        "Storing the large datasets"
    ],
    "Best practices": [  # label: SERVC1/8
        "Am besten geeignete Datenformate",
        "Standrad vocaboularies"
    ],
    "Metadata enrichment of research data":[ # label: SERVC1/4
        "Umsetzung von Software um Metadaten sinnvoll mit den Datensätzen zu kombinieren"
    ]
}

# version 1.0 by Leon 20/01/2022
# Q: Please name the three most important software applications that you use for your research.
# Leon, 2022-01-22: I suggest merging the mapping for all three RMPR10
# subquestions because that's easier to work with and because we did not want
# to distinguish between them anyway, as far as I can remember.
# v 1.1 reviewed by Markus 09/02/2022
# v 1.2 reviewed by Sangeetha
# v 1.3 MK: mapped 486 missing entries (from survey data columns RDMPR10_2 and RDMPR10_3)
RDMPR10 = {
    "toRemove": [
        0,
        "Wiki",
        "Confluence",
        "verstehe die Frage nicht, meinen Sie die Namen der selbstenwickelten Software oder ist Anwendungssoftware, die mich unterstützt?",
        "-",
        "Personenbezogenen Daten",
        "gerätespezifische Software",
        "netCDF", #is a dataformat, not a software.
        "JPG",
    ],
	"Other": [  
        # not specific enough
        "intel compiler",
        "gerätespezifische Software",
        "Geräte-spezifische Software",
        "Gerätesoftware",
		"Diverse CAD Software",
        "Structurlösre",
        "Gerätespezifische Steuerungs- und Datenerfassungssoftware",
        "Geräte spezifische Bild- und E-phys Daten Aufnahme",
        "Hersteller-Steuer-Software",
        "Anwendungsspezifische Software",
        "Datenintegrations-Tools",
        "GIS",
        "Imaging Analyses",
        "Microsoft",
        "Simulationssoftware",
        "IDE, Compiler, Interpreter",
        "Web-broswer (GitLab, Mattermost, etc)",
        "Eclipse",
        "CartoDB",
        "netCDF tools",
        "Bildverarbeitungsoftware",
        "Network Analyses" , 
        "Netzgenerator",
        "Videobearbeitung  z.B. PFV",
        # the following cannot be uniquely mapped because they contain multiple answers in the same string
        # MK: find a way to map these to multiple answers?
        "NET und C#", 
        "Open source software (e.g. bwa) and R/Bioconductor packages",
        "image and text processing, latex, gimp, ...",
        "GTlab/Python/Excel (Visualisierung)",
        "STATA, R",
        "Ocean Data View, ArcGIS",
        "Microcal Origin",
        "Adobe (Illustrator) & Microsoft packages (Word, Excel, Power point)",
        "with devices",
        # one might map the following to "own software", as once can assume that own scripts / software is produced
        "Enwicklungsumgebungen",
        "VS Code/Jupyter Notebook",
        "Python + Jupyter/VSCode",
        "Matlab/Python",
        "matplotlib/gnuplot",
        "Matlab / Python",
        "PyCharm / VSCodium",
        "Visual Studio & Fortran",
        "Rstudio, FlowJo",
        "ImageJ, FiJi, Java",
        "SPSS / R",
        "ferret shell python octave scripts",
        "Programmiersprachen (Python, R)",
        ".NET und C#"
    ],
    "Office Software (unspecified)": [  # added unspecif. to distinguish from MS Word and Excel
        "Office",
        "MS Office",
        "MSOffice",
        "Office tools",
        "Office Anwendungen",
        "Office-Packet",
        "office suite",
        "MS Office (Excel, Access)",
        "Access, Excel, csv",
        "Office (hauptsächlich Excel, Word & PowerPoint)",
        "Microsoft Office",
        "Libreoffice",
        "Libreoffice/MS Office"
    ],
    "Excel (Spreadsheets)": [  # TODO: discuss integration into "Office Software"
        "MS Excel",
        "MS-Excel",
        "Excel",
        "Exel",
        "MS Excel (als Einziges (fast) immer verfügbar)",
        "Microsoft Excel",
        "Spreadsheets",
        "Tabellenkalkulation",
        "XLS", 
        "xlsx"
    ],
    "Word": ["Word", "MS Word", "Microsoft Word"],  # moved
    "PowerPoint": ["Power Point","PowerPoint"],  # moved
    "Python": [
        "python",
        "Phython",
        "Pyton",
        "Phyton",
        "Python (everything to develop/run)",
        "Python Libraries",
        "Skripte zur Auswertung (v.a. Python)",
        "Pandas",
        "Python / Pandas",
        "Python (pandas)",
        "Python (Anaconda)",
        "Anaconda / Python",
        "Anaconda/Python",
        "Anaconda",
        "PyCharm",
        "PyCharm Community",
        "Python-IDE (PyCharm)",
        "Python IDEs",
        "Spyder",
        "Python/Pycharm",
        "PyCharm",
        "Pythonskripte",
        "Python Skripts",
        "Python scripts",
        "Python Scripting",
        "Python Environment Spyder",
        "Spyder (Python environment)",
        "Python (ie. Spyder)",
        "Spyder (running various analysis scripts)",
        "Python (Datenauswertung)",
        "python/jupyter",
        "Python, Jupyter notebooks",
        "matplotlib/gnuplot",
        "Scientific Python packages",
        "python (bumpy/scipy/pandas)",
        "Python based open source software (e.g. scanpy)",
        "matplotlib",
        "Python and libraries",
        "Python (Libraries)",
        "python3.x",
        "Python 3.8",
        "python IDE",
        "Python3 mit numpy + scipy",
        "Numpy/Pytorch/Panadas",
        "Numpy/Pytorch/Panadas...",
        "python and python-based specific applications such as pyFAI, pdfgetX3",
        "numpy",
        "scipy",
        "oemof (python)",
        "Python, Obspy",
        "Python-XGBoost"
    ],
    "LabVIEW": [
        "Labview",
        "Labview (in-house application for measurement setup)",
        "National Instruments Labview", 
        "LISE", # Labview (inhouse-dev. software at HZB),
        "LabView", 
        "LabView (Data Aufnahme)", 
        "LabView (Datenerfassung)",
        "LabView Selfmade-Tools"
    ],
	# Leon, 2022-01-20: jupyter probably means that they use python but they
    # could theoretically also use other languages. TODO Should we group
    # jupyter-esque things under "Python" anyway?
    # Markus, 2022-01-09: I vote not to. May include R, Python, Julia, ...
    "Jupyter": ["Jupyter Lab",
        "Jupyter",
        "Jupyter Notebook",
        "Jupyther Notebook",
        "jupyter",
        "Jupyter notebooks/lab",
        "VS Code/Jupyter Notebook",
        "Jupyter Notebooks + Python",
        "Python (Jupyter)",
        "Python/Jupyter-Notebook",
        "python/jupyter",
        "Python Notebook",
        "Python (Pandas, Numpy, Matplotlib, Jupyter)",
        "Python (Jupyter Notebook, Pandas)",
        "\"Python,Jupyter notebooks\"",
        "Jupyter Notebook (Python)", 
        "Juypter"
    ],
    "Visual Studio": [
        "Visual Studio",
        "Visual Studio C++",
        "VSCode",
        "VS Code",
        "Visual Studio Code",
        "Visual Studio (Code)"
    ],
	"Own software": [
        "https://github.com/momentoscope/hextof-processor",
        "in House Entwicklungen",
        "[Homemade software]",
        "selbst geschrieben",
        "own software for moving base simulator",
        "selbst geschriebene Software",
        "Custom software",
        "selbstprogrammiertes Messprogramm",
        "self-developed app",
        "Eigens programmierte Softwareanwendung",
        "selbstentwickeltes eResearch System",
        "Self written simulation code",
        "Qt Selfmade-Tools",
        "Self-made acquisition software (combines camera and mass-spectrum ADC into HDF5 file)",
        "own inhouse software",
        "Selbstentwickelte Software (C++)",
        "own software aeromechanics",
        "Selbst-entwickelte Software zur Bilddaten-Analyse",
        "selbsprogrammiertes Upload und Archivierungsprogramm für Labormessdaten",
        "own Ptychography software",
        "ADLER (in-house software)", 
        "own 3DXRD software",
        "own Python and Rust libraries",
        "proprietary C-codes",
        "Isaac (Messprogramm, Eigenentwicklung)",
        "interne \"Prototypen\"",
        "Eigens geschriebene Modelle (F90)",
        "DrSpine (Eigenentwicklung)n F90",
        "datreat (Eigenentwicklung) F90", "Datreat",
        "CFD (Eigenentwicklung - FLOWer,TAU,CODA)",
        "Flugmechanikverfahren (Eigenentwicklung von Airbus Helicopters)",
        "selbstgeshrieben",
        "Eigne Modellentwicklungen",
        "Eigene Software",
        "nse (Eigenentwicklung) C",
        "in-house codes THETA-SPRAYSIM",
        "in-house C++ tools",
        "ATLAS specific"
    ],
    "C/C++": ["C/C++", "C","C++", "C++ compiler","C++-compiler","Visual C","C++ (everything to develop/compile)"],
    "Database Management System": [
        "PostGreSQL/ MSAccess", 
        "SQL", 
        "SQL Developer", 
        "PGAdmin", 
        "PostGIS", 
        "Oracle", 
        "Access", 
        "MongoDB", 
        "PostgreSQL",
        "DBeaver",
        "InfluxDB",
        "Influx",
        "MySQL",
        "Oracle RDBMS",
        "Oracle DB",
        "Microsoft SQL Server Management Studio"
    ],
	"ACD Labs" : ["ACD Labs"],
    "ACD structure elucidator" : ["ACD structure elucidator"],
    "Activity Browser" : ["Activity Browser"],
    "Adobe Acrobat" : ["acrobat"],
    "Adobe Illustrator": ["Adobe Illustrator", "Illustrator"],
    "Affinity Designer" : ["Affiity Designer"],
    "Agilent HPLC" : ["Agilent HPLC"],
    "Agilent tape station analysis software" : ["tape station analysis software agilent"], # please check
    "AiiDA" : ["AiiDA", "Aida", "aiida.net (Automated Interactive Infrastructure and Database for Computational Science)", "AiiDA-KKR", "iffaiida.iff.kfa-julich.de"],
    "AliPhysics" : ["AliPhysics"],
    "Allpix2" : ["Allpix2"],
    "alphaMELTS" : ["alphaMELTS"],
    "Altair HyperWorks" : ["Altair HyperWorks"],
    "Altium Designer": ["Altium Designer","altium"],
    "Amira" : ["Amira"],
    "AMOS" : ["AMOS"],
    "ANSYS": ["ANSYS","Ansys Workbench","Ansys Fluent","ansys meshing","ansys cfx"],
    "Apache-SOLR" : ["Apache-SOLR"],
    "ArcGIS": ["Arcgis","ArcMap"],
    "ASC" : ["ASC"],
    "Aspen Custom Modeler" : ["Aspen Custom Modeler","Aspen Plus"],
    "ASTRA" : ["ASTRA"],
    "Astropy / Gammapy" : ["Astropy and Gammapy"],
    "Audacity" : ["Audacity"],
    "Autodesk Inventor": ["Autodesk Inventor"],
    "Avizo": ["Aviso", "Avizo"],
    "AZtec (Oxford Instruments)" : ["AZtec (Oxford Instruments)"],
    "b2000++pro" : ["b2000++pro"],
    "BayesServer" : ["BayesServer"],
    "Bexis" : ["Bexis"],
    "BioLection" : ["BioLection"],
    "Blender": ["Blender","BlenderProc"],
    "BornAgain" : ["BornAgain"],
    "BOWTIE2" : ["BOWTIE2"],
    "Bruker DataAnalysis" : ["Bruker DataAnalysis"],
    "Bruker Diffrac.eva" : ["Diffrac.eva (Bruker)"],
    "Bruker Nanoscope" : ["Bruker Nanoscope"],
    "Bruker TopSpin" : ["Bruker TopSpin"],
    "BWA" : ["BWA"],
    "CaFuR" : ["CaFuR"],
    "CAM" : ["CAM"],
    "Carla" : ["Carla"],
    "cascade" : ["cascade"],
    "CATENA" : ["CATENA"],
    "CATIA CAD-software" : ["CAD: Catia", "CAD (CATIA)", "CATIA", "Catia V5", "Catia", "CatiaV5"],
    "CCP4 Programms" : ["CCP4 Programms"],
    "cdo": ["cdo"],
    "Cellpose" : ["cellpose"],
    "Cellranger" : ["Cellranger"],
    "Centaur": ["Centaur","Centaur / Solar"],
    "CERN corryvreckan" : ["CERN corryvreckan"],
    "CFD Software (e.g. TecPlot, ParaView)" : ["CFD Software", "CFD Löser", "CFD-Simulation", "CFD Visualisierungssoftware (e.g. TecPlot, ParaView)"],
    "ChemDraw": ["ChemDraw"],
    "chemkin" : ["chemkin"],
    "Citavi": ["Citavi"],  # moved
    "Clojure(Script) (Programmiersprache)" : ["Clojure(Script) (Programmiersprache)"],
    "CODA" : ["CODA"],
    "Command line (scripts)": ["Bash", "Shell-Scripte", "Unix-Skripte, z.B. csh, awk","Shell-Scripting", "Bash Skripte","Command Line","MobaXTerm","Linux command line","Shell Script","Shell Scripts"],
    "Communication apps":["zoom","Outlook (Datenaustausch)","Outlook"],
    "COMSOL Multiphysics" : ["COMSOL", "Comsol Multiphysics", "Comsol"],
    "Coot" : ["Coot"],
    "CORSIKA" : ["CORSIKA"],
    "CryoSPARC" : ["CryoSPARC"],
    "Crysallis" : ["Crysallis"],
    "CST Studio Suite" : ["CST Studio Suite"],
    "CVI42" : ["CVI42"],
    "DataLad" : ["DataLad", "Datalad"],
    "DaVis": ["DaVis"],
    "DAWN" : ["DAWN"],
    "Delphi": ["Delphi","Delphi (Programmierungebung für Messprogramme)"],
    "DETCHEM" : ["DETCHEM"],
    "DIAdem (National Instruments)" : ["DIAdem (National Instruments)", "DIADEM"],
    "DIAMOND" : ["DIAMOND"],
    "Digital micrograph" : ["digital micrograph"],
    "Dioptas" : ["Dioptas"],
    "Diverse DAQs an FELs" : ["Diverse DAQs an FELs"],
    "Docker/Podman" : ["Docker/Podman"],
    "DOCS" : ["DOCS"],
    "DPDAK" : ["DPDAK"],
    "dSpace ControlDesk" : ["dSpace ControlDesk"],
    "Dymola": ["Dymola"],
    "Dynaworks" : ["Dynaworks"],
    "EBSILON Professional" : ["EbsilonProfessional"],
    "EC-Lab (BioLogic)" : ["EC-Lab (BioLogic)"],
    "Elastic Search" : ["Elastic Search"],
    "Electronic Lab Notebook (ELN)" : ["Electronic Lab Notebook (ELN)", "ELN", "Elektronische Laborbücher"],
    "elegant" : ["elegant"],
    "EMAC atmospheric model" : ["Atmosphärenmodell EMAC"],
    "emacs" : ["emacs", "EMAC", "Emacs", "GNU Emacs (Notizen, knowledge base, Literaturverwaltung, LaTeX-Editor, IDE)"],
    "Energy systems modelling software (proprietary)" : ["Energiesystemmodellierungswerkzeug (externes, spezifisches Produkt)"],
    "Engauge digitizer" : ["Engauge digitizer"],
    "Enterprise Architect": ["Enterprise Architect"],
    "ENVI" : ["ENVI"],
    "EPICS" : ["various EPICS tools"],
    "EPLAN": ["EPLAN"],
    "ESMF" : ["ESMF"],
    "ETL Tools" : ["ETL Tools"],
    "Eventdisplay" : ["Eventdisplay (software for gamma-ray observatories; https://github.com/Eventdisplay/Eventdisplay)"],
    "FABM" : ["FABM"],
    "FactSage": ["FactSage"],
    "fastai" : ["fastai"],
    "ferret" : ["ferret"],
    "FE-Solver" : ["FE-Solver"],
    "Fiji": ["Fiji","Fiji / Imagej","Fiji (ImageJ)"],
    "Finite Elements software" : ["Finite Elements (Abaqus, LSDyna)"],
    "Fit2D" : ["Fit2D"],
    "Fityk" : ["Fityk"],
    "Flash" : ["Flash"],
    "Fleur" : ["Fleur"],
    "FlowFit" : ["FlowFit"],
    "FlowJo": ["FlowJo"],
    "FLUKA" : ["FLUKA", "Fluka", "FLUKA (INFN)", "FLUKA code"],
    "fMRIPrep" : ["fMRIPrep"],
    "Fortran": ["fortran", "F90 Code","Modellsimulationen (Fortran Code)", "fortran code", "Fortran Programme"], 
    "Foxit PDF" : ["Foxit PDF editor"],
    "FragMAXapp" : ["FragMAXapp"],
    "FreeSurfer": ["Freesurfer"],
    "FSDM" : ["FSDM"],
    "fsl" : ["fsl"],
    "Fullprof" : ["Fullprof"],
    "g++" : ["g++"],
    "Galaxy platform" : ["Galaxy platform"],
    "GAMS Studio" : ["GAMS Studio", "GAMS"],
    "gcc": ["gcc"],
    "GEANT4": ["GEANT4"],
    "Geneious" : ["Geneious"],
    "Generic Mapping Tools" : ["Generic Mapping Tools"],
    "Genesis 1.3" : ["Genesis 1.3"],
    "GENPLOT" : ["GENPLOT"],
    "Geochemist's Workbench": ["Geochemist's Workbench"],
    "GHOST" : ["GHOST"],
    "GIMP": ["Gimp"],
    "Git": ["Gitlab","git","SVN & Git","Github", "Git + Gitlab", "Git", "Git + Gitlab"],
    "GMT" : ["GMT"],
    "Gnu Octave" : ["Gnu Octave","octave"],
    "gnuplot": ["gnuplot"],
    "GOM ATOS Professional" : ["GOM ATOS Professional"],
    "Google Docs": ["Google Docs"],
    "Google Earth Engine": ["Google Earth Engine", "GoogleEarthEngine","Google Earth"],
    "Grafana": ["Grafana"],
    "GraphPad": ["GraphPad","Graph Pad Prism","GraphPadPrism","Graph Pad","GraphPad Prism"],
    "GROMACS" : ["GROMACS", "Gromacs", "GROMACS oder ähnliches"],
    "GTlab (Simulation)" : ["GTlab (Simulation)", "GTlab"],
    "Guix" : ["Guix"],
    "Gwyddion" : ["Gwyddion"],
    "HD Model" : ["HD Model"],
    "HDF5-suite" : ["HDF5-suite"],
    "Hypermesh" : ["Hypermesh"],
    "iCal" : ["Datenerfassung Laborgeräte (z.B. iCal)"],
    "ICEMCFD" : ["ICEMCFD"],
    "ICON-ART" : ["ICON-ART"],
    "IDL": ["IDL"],
    "IGOR": ["IGOR","Igor Pro 7","IGOR 64", "IGOR Pro","IGORPro"],
    "iLastik" : ["iLastik"],
    "Illumina Sequencing Analysis Viewer" : ["Illumina Sequencing Analysis Viewer"],
    "ImageD" : ["ImageD"],
    "ImageJ": ["ImageJ", "Image J"],
    "indico" : ["indico"],
    "Inkscape": ["Inkscape", "Inkscape (for generating figures)"],
    "IntelliJ IDEA" : ["IntelliJ IDEA"],
    "Internet explorer" : ["explorer"],
    "IOGAS" : ["IOGAS"],
    "Ipkiss" : ["Ipkiss"],
    "ISATIS" : ["ISATIS"],
    "ISPyB" : ["ISPyB"],
    "JANA" : ["JANA"],
    "JASP" : ["JASP"],
    "Java": ["Java"],
    "JavaScript": ["OpenLayers","Digital Globe (Cesium)"],
    "JCMwave" : ["JCMwave (FEM solver)"],
    "JetBrains Toolbox" : ["JetBrains Toolbox"],
    "JMP" : ["JMP"],
    "JScatter" : ["JScatter"],
    "JuFold" : ["JuFold"],
    "juKKR" : ["juKKR", "jukkr.fz-juelich.de (density functional theory based on Korringa-Kohn-Rostoker (KKR) Green function method)"],
    "Julia" : ["Julia"],
    "JURASSIC" : ["JURASSIC"],
    "Kaapana" : ["Kaapana"],
    "Kadi4Mat" : ["Kadi4Mat"],
    "Kafka": ["Kafka"],
    "kdevelop" : ["kdevelop"],
    "Keyence Imager" : ["Keyence Imager"],
    "Kicad" : ["Kicad"],
    "KNIME" : ["KNIME"],
    "LAMMPS" : ["LAMMPS"],
    "LaTeX": ["latex","Texmaker","LaTeX-Compiler", "TeX","Overleaf","TeXnicCenter","LaTex compiler"],
    "Leapfrog Geo": ["Leapfrog"],
    "Limesurvey": ["Limesurvey"],
    "Linux": ["Linux", "Linux bzw. WSL"], # Leon, 2022-01-20: WSL = Windows subsystem for Linux
    "Loupe browser" : ["Loupe browser"],
    "LS-DYNA" : ["LS-DYNA"],
    "Machine Learning Libraries" : ["Machine Learning Libraries", "Various ML algorithms (such as XGBoost)", "scikit-learn", "Tensorflow", "Weka (ML)"],
    "MacOS" : ["MacOS"],
    "MADX" : ["MADX"],
    "MANTID" : ["MANTID", "Mantid"],
    "Maple" : ["Maple"],
    "Masshunter" : ["Masshunter"],
    "Mathematica": ["Mathematica", "Matematica"],
    "MATLAB / Simulink": ["Matlab (running various analysis or simulation scripts)","MATLAB","matlap","Matlab/Simulink","Matlab/ Simulink","Matlab / Simulink", "matlab/spm", "Simulink"],
    "MaxQDA" : ["MaxQDA", "maxqda"],
    "MaxQuant" : ["MaxQuant"],
    "MBS" : ["MBS"],
    "Melts" : ["Melts"],
    "MESAP" : ["MESAP"],
    "MicroCal" : ["MicroCal"],
    "Microscopy Image Browser" : ["Microscopy Image Browser"],
    "Microsoft Visio" : ["Microsoft Visio"],
    "Microscopy software (Leica, Zeiss)" : ["Mikroskopie Software (Leica, Zeiss)"],
    "Mimics" : ["Mimics"],
    "MinKNOW" : ["MinKNOW"],
    "MITK" : ["MITK", "MITk"],
    "Mocadi" : ["Mocadi"],
    "Modelica" : ["Modelica"],
    "Modelsim" : ["Modelsim"],
    "mosaik" : ["mosaik"],
    "MOSSCO" : ["MOSSCO"],
    "MPTRAC" : ["MPTRAC"],
    "MRIcron/NPM / NiiStat" : ["MRIcron/NPM / NiiStat"],
    "MS visual C++" : ["MS visual C++"],
    "MSC Nastran" : ["MSC Nastran"],
    "MSC Patran" : ["MSC Patran"],
    "Mumax" : ["Mumax"],
    "MZmine" : ["MZmine"],
    "NanoSuite" : ["NanoSuite"],
    "napari" : ["napari"],
    "NAPEOS" : ["NAPEOS"],
    "Nastran" : ["Nastran"], # check if it's the same as "MSC Nastran"
    "NCL" : ["NCL"],
    "NCO" : ["NCO", "nco"],
    "Imaging software (Nikon and Leica)" : ["Nikon and Leica imaging software"],
    "NIS-Elements Viewer" : ["NIS-Elements Viewer"],
    "Notepad++": ["notepad++"],
    "Notion" : ["Notion"],
    "Nyan (Blickdaten)" : ["Nyan (Blickdaten)"],
    "OCR Reader" : ["OCR Reader"],
    "Omnigraffle" : ["Omnigraffle"],
    "OPAL" : ["OPAL"],
    "OpenCV": ["OpenCV","Open-CV"],
    "OpenFOAM": ["OpenFOAM"],
    "openQCD" : ["openQCD"],
    "OpticStudio" : ["OpticStudio"],
    "Optimierer" : ["Optimierer"],
    "OPUS" : ["OPUS", "Opus"],
    "ORCA" : ["ORCA", "orca"],
    "OriginLab": ["Origin", "OriginLab", "Origin 2020", "Origin Lab.", "Origin Labs", "Origin Pro", "OriginPro", "Origin-Pro"],
    "ORS Dragonfly" : ["ORS Dragonfly"],
    "OVITO" : ["OVITO", "Ovito"],
    "Pace3D": ["Pace3D"],
    "PandaROOT" : ["PandaROOT"],
    "Panoply" : ["Panoply"],
    "Paraview": ["Paraview"],
    "Parsl" : ["Parsl"],
    "Partnersoftware" : ["Partnersoftware"],
    "partsival" : ["partsival"],
    "PDVisA/ELAB" : ["PDVisA/ELAB"],
    "Peridigm" : ["Peridigm"],
    "PERMAS" : ["PERMAS"],
    "Perple_X" : ["Perple_X"],
    "Perseus" : ["Perseus"],
    "Phenix" : ["Phenix", "phenix"],
    "Phoenix" : ["Phoenix"],    # check whether it's the same as Phenix
    "Photoshop": ["Photoshop"],
    "PHREEQC" : ["PHREEQC", "PhreeqC"],
    "PIA" : ["PIA"],
    "PIConGPU" : ["PIConGPU","PIConGPU (PIC)"],
    "PIVview" : ["PIVview"],
    "PLINK" : ["PLINK", "Plink"],
    "pmod" : ["pmod"],
    "Pointwise" : ["Pointwise"],
    "POV-Ray" : ["POV-Ray"],
    "PowerFactor" : ["PowerFactor (DIgSILENT)", "Powerfactory"], # check whether it's "PowerFactor" or "PowerFactory"
    "Prism" : ["Prism"],
    "Profex (Rietveld)" : ["Profex (Rietveld)"],
    "Protein Data Base" : ["Protein Data Bases"],
    "Psyplot" : ["Psyplot"],
    "PTC Creo": ["PTC Creo Parametric"],
    "PubMed" : ["https://pubmed.ncbi.nlm.nih.gov/"],
    "pyFAI (ESRF)" : ["pyFAI (ESRF)"],
    "PyMca": ["pyMCA"],
    "PyPSA" : ["PyPSA"],
    "PYTHIA" : ["PYTHIA"],
    "PyTorch": ["PyTorch","pytrorch"],
    "QCoDeS" : ["QCoDeS"],
    "QD MultiVu" : ["QD MultiVu"],
    "QGIS/GDAL": ["Qgis","GDAL","gdal/ogr"],
    "Qt Creator" : ["Qt Creator", "Qt Entwicklungsumgebung"],
    "qtiKWS/qtiSAS" : ["qtiKWS/qtiSAS"],
    "Qtiplot" : ["Qtiplot"],
    "qtiSAS" : ["qtiSAS", "qtisas"],
    "QuantStudio Real-Time PCR Software" : ["QuantStudio™ Real-Time PCR Software"],
    "Quartus Pro" : ["Quartus Pro"],
    "R": ["R","R-Studio","Rstudio","R studio","R-statistics","R Statistik"],
    "RailSys" : ["RailSys"],
    "RAxML" : ["RAxML"],
    "RAY-UI" : ["RAY-UI"],
    "RCE" : ["RCE"],
    "refine" : ["refine"],
    "Relion" : ["Relion"],
    "Roadrunner" : ["Roadrunner"],
    "roboDK" : ["roboDK"],
    "RooFit" : ["RooFit", "Roofit"],
    "ROOT framework": ["ROOT", "CERN ROOT", "ROOT analysis software", "ROOT (CERN)","ROOT (CERN, C++)", "ROOT framework"],
    "ROS": ["ROS","Robot Operating System (ROS)","Robot Operation System"],
    "Rover" : ["Rover"],
    "SAMtools" : ["SAMtools"],
    "SAS": ["SAS"],
    "SASSENA" : ["SASSENA"],
    "SATAN" : ["SATAN"],
    "Savu (from Diamond Light Source)" : ["Savu (from Diamond Light Source)"],
    "SCAPS 1D" : ["SCAPS 1D"],
    "SCIWHEEL" : ["SCIWHEEL"],
    "SDS (Applied Biosystems)" : ["SDS (Applied Biosystems)"],
    "SeisComP" : ["SeisComP"],
    "SerialEM mit Imod und etomo" : ["SerialEM mit Imod und etomo"],
    "SES analyser control software" : ["SES analyser control software"],
    "shepard" : ["shepard"],
    "Siesta" : ["Siesta"],
    "SIMION" : ["SIMION", "simion"],
    "Slurm" : ["Slurm", "slurm"],
    "Smilei" : ["Smilei", "Smilei (PIC)"],
    "Snakemake": ["Snakemake"],
    "SNAP": ["SNAP"],
    "SnapGene" : ["SnapGene", "Snapgene"],
    "Solidworks" : ["Solidworks"],
    "SPEC" : ["SPEC", "spec scientific software", "Spec"],
    "SPECs Lab Prodigy" : ["SPECs Lab Prodigy"],
    "SpectraRay 4" : ["SpectraRay 4"],
    "spectroflow" : ["spectroflow"],
    "spfit/spcat / pgopher" : ["spfit/spcat / pgopher"],
    "Spirit" : ["Spirit"],
    "spm" : ["spm"],
    "SPSS": ["SPSS","SPSS (und Erweiterungen wie AMOS)"],
    "srim" : ["srim"],
    "STAC8 code" : ["STAC8 code"],
    "STAR" : ["STAR"],
    "Stata" : ["Stata"],
    "Statistica" : ["Statistica", "STATISTICA", "STATISTIKA"],
    "Stream Dektop" : ["Stream Dektop"],
    "SUMO": ["SUMO","Simulation of urban mobility"],
    "Synchrotron Radiation Workshop (SRW)" : ["Synchrotron Radiation Workshop (SRW)"], # check whether that is really software
    "SyngoVia" : ["SyngoVia"],
    "TAU" : ["Tau", "TAU" , "DLR Tau Code", "DLR Tau Solver / FlowSim", "DLR Code TAU"],
    "Tecplot": ["TecPlot", "Tecplot (Datenvisualisierung)"],
    "TESPy" : ["TESPy"],
    "TestExpert II ZwickRoell" : ["TestExpert II ZwickRoell"],
    "Text editor (unspecified)": ["source code editor", "Texteditor", "Texteditoren / IDE"],
    "THATec" : ["THATec"],
    "Thermo Chromeleon" : ["Thermo Chromeleon"],
    "TIA Portal (Siemens Automation Software)" : ["TIA Portal (Siemens Automation Software)"],
    "TOPAS" : ["TOPAS"],
    "TRACE" : ["TRACE"],
    "Tracefinder" : ["Tracefinder"],
    "Transkriber" : ["Transkriber (Sprachdaten)"],
    "Turbomole" : ["Turbomole"],
    "UCESB" : ["UCESB"],
    "Ultimaker Cura" : ["Ultimaker Cura"],
    "Unity" : ["Unity"],
    "Unreal Engine" : ["Unreal Engine"],
    "VASP" : ["VASP"],
    "vesta" : ["vesta"],
    "Vicon Nexus" : ["Vicon Nexus"],
    "VIM" : ["VIM", "vim"],
    "Visio" : ["Visio"],
    "Vitess/McStas" : ["Vitess/McStas"],
    "VIVADO" : ["VIVADO"],
    "vmd" : ["vmd"],
    "Wavemetrics Igor" : ["Wavemetrics Igor (Datenauswertung)", "Wavemtrics Igor"],
    "WiNDF" : ["WiNDF"],
    "WinGupix" : ["WinGupix"],
    "Wireshark" : ["Wireshark"],
    "xarray" : ["xarray"],
    "Xcrysden" : ["Xcrysden"],
    "XDibias" : ["XDibias"],
    "XDS (X-ray data processing)" : ["X-ray data processing XDS", "XDS", "xds"],
    "XDSAPP" : ["XDSAPP"],  # check whether it's the same as XDS
    "xFitter" : ["xFitter"],
    "X-plane" : ["X-plane"],
    "XRootD" : ["XRootD"],
    "Zeiss SmartSEM" : ["Zeiss SmartSEM"],
    "ZEMAX" : ["ZEMAX", "Zemax"],
    "ZEN" : ["Zen", "ZEN 3.0"]
}

# first version: ?
# reviewed by MK 2022/02/16
# Q: In which repositories have you published your data?
DTPUB5 = {
    "toRemove": ["Wikis",
                 "none yet, but would use:",
                 "Zotero",
                 "Arxiv",
                 "ELO"
                 ],
    "Institutional server": [
        "Institutswebseite",  # moved here
        "DLR FTP",  # moved here
        "Projekt-Homepage",  # moved here
        "public, self-hosted webservers",  # moved here
        "Self-hosted repositories",  # maybe move to "Institutional website" ?
        "Eigenes Repository",  # moved here
        "group-owned public database",  # moved here
        "Custom organized, organized via CERN",  # moved here
        "Institutional/departmental",  # moved here
        "intern",  # moved here
        "Institut"  # moved here
    ],
    "GSI repository": [
        "GSI internal (ALADiN)",
        "ALADiN Website @ GSI",
        "GSI internal (FOPI)"
    ],
    "HZB ICAT": [
        "DOI vom HZB",
        "data.helmholtz-berlin.de"
    ],
    "FIZ Karlsruhe": [
        "FIZKarlsruhe" 
    ],
    "Zenodo": ["Zenodod",
               "Zenodo",
               "ZENODO",
               "https://zenodo.org/record/3628356",
               "https://zenodo.org/",
               "zenodo",
               "zenodo.org"
               ],
    "RCSB PDB": ["ww.rcsb.org",
                 "Protein Data Bank (RCSB-PDB)",
                 "pdb",
                 "PDB"],
    "WDCC": ["WDC Climate @DKRZ",
             "CERA Datenbank am DKRZ",
             "CERA Datenbank",
             "DKRZ",
             "WDCC"],
    "GeoNetwork": ["UFZ geonetwork"],
    "BKG": ["BKG"],
    "TORE": ["TUHH Open Research (TORE)"],
    "RODARE": ["RODARIS",
               "RODARE",
               "RODARE (Rossendorf data repository)",
               "Rodare (HZDR)",
               "HZDR intern",  # moved here
               "Rodare"],
    "ROBIS": ["ROBIS",
              "RoBis"],
    "PUBLISSO": ["Publisso"],
    "ProteomeXchange": ["ProteomeXchange",
                        "Pride",
                        "Proteome Exchange",
                        "Pride (Proteomics identification database)"],
    "IRRMC": ["proteindiffraction.org"],
    "PANGAEA": ["PANGAEA",
                "Pangaea"],
    "OSF": ["osf.io",
            "OSF"],
    "OpenOrganelle": ["OpenOrganelle"],
    "Open Energy Platform": ["OEP"],
    "IMPC": ["Mousephenotype.org"],
    "Materials Cloud": ["materialscloud archive",
                        "materials cloud archive",
                        "materialscloud.org"],
    "ILDG": ["ILDG"],
    "NASA LaRC": ["https://www-air.larc.nasa.gov/"],
    "HEPData": ["HepData"],
    "Helmholtz Coastal Data Center": ["HCDC"],
    "Gitlab / GitHub": [
        "Gitlab",
        "GitLab",
        "GitHub",
        "Github",
        "github",
        "Git-hub",
        "Git",
        "github/gitlab",
        "https://github.com/PeriDoX/PeriDoX"],
    "HALO database": ["HALO-DB",
                      "HALO Database",
                      "DLR HALO"],
    "GLIMS": ["GLIMS"],
    "GFZ data services": ["GFZ Data services"],
    "GEO": ["GEO (Gene Expression Omnibus)",
            "GEO",
            "Gene Expression Omnibus (GEO)",
            "Gene expression omnibus"],
    "figshare": ["figshare",
                 "Fig share",
                 "Figshare"],
    "FAIRDOMHub": ["FAIRDOMHub"],
    "EudraCT": ["EudraCT"],
    "Copernicus Open Access Hub": ["ESA data hub",
                                   "Copernicus Atmospheric Data Store"],
    "EOC Geoservice": ["Eoc geoservice",
                       "EOC Geoservice"],
    "EOC DIMS": ["EOC DIMS"],
    "EOC IMPC": ["EOC IMPC(https://impc.dlr.de/products)"],
    "ENA": ["ENA",
            "EMBL-EBI",
            "EMBL ENA",
            "ENA (Euopean Nucleotide Archive)",
            "European Nucleotide Archive"],
    "EGA": ["EGA"],
    "e!DAL": ["e!DAL: electronic Data Archive Library"],
    "CDDIS": ["CDDIS"],
    "BMRB": ["Biological Magnetic Resonance Data Bank (BMRB)"],
    "ArrayExpress": ["ArrayExpress"],
    "4DN": ["4DN"],
    "CDMS": ["cdms"],
    "ClinicalTrials.gov": ["Clinicaltrials"],
    "dbGAP": ["dbGaP"],
    "EMDB": ["Electron Microscopy Database (EMDB)"],
    "GEOFON": ["GEOFON",
               "GEOFON Seismological Archive"],
    "HEASARC": ["HEASARC"],
    "GenTaR": ["https://www.gentar.org/tracker/#/"],
    "iReceptor": ["iReceptor / AIRR Data Commons"],
    "KITopen": ["KITopen"],
    "MetaboLights": ["Metabolights"],
    "NSSDC": ["NASA Space Science Data Facility"],
    "Qucosa": ["Qucosa"],
    "SRA": ["SRA"],
    "ePrints Soton": ["University of Southampton Institutional Repository"],
    "B2SHARE": ["b2share",
                "b2share.fz-juelich.de"],
    "EMPIAR": ["EMPIAR (raw microscopy data)"],
    "UCSC Genome Browser": ["UCSC genome browser"],
    "TERENO": ["TERENO (https://ddp.tereno.net/ddp/dispatch?searchparams=freetext-demmin)"],
    "GIN": ["Gin"],
    "FZJ Datapub": [
        "https://datapub.fz-juelich.de/slcs/",  # moved here
        "Jülich Data"  # moved here
    ],
    "DataONE": ["DataOne"],  # moved here
    "AIRR Data Commons (ADC)": ["ADC (AIRR Data Commons)"],  # moved here
    "ESA Data Archive": ["ESA SSA (https://swe.ssa.esa.int/tio_dashboard)"],
    "Other": ["SVN",
              "Repositorien",  # moved here
              "Data journals",  # moved here
              "IGN",  # unclear
              "GSO"  # unclear
              ]
}

# v1.0: mapped by MK on 2022/04/01
# Reviewed
# Q: Please select the methods used to generate your research data.
RSDP2_other = {
    "toRemove": [
        0,
        "blabla"
        ],
	"Imaging": [ 	#label: RSDP2/1
	],
	"Analytical methods": [	#label: RSDP2/2
        "Transcript expression",
        "Large scale experiment at the LHC",
        "Teilchendetektoren",
        "exp. Daten aus Beschleunigerexperimenten",
        "Streumethoden",
        "Neutronenstreuung",
        "neutron scattering",
        "streumethpden",
        "Neutronenstreuung",
        "scattering",
        "Teilchendetektoren",
        "X-ray diffraction",
        "Diffraction",
        "particle detection",
        "Direkte Messung physikalischer Parameter",
        "NGS (Next Generation Sequencing)"
	],
	"Simulations": [	#label: RSDP2/3
        "Artificial Generation"
	],
	"Sample synthesis and preparation": [	#label: RSDP2/4
        "Biochemische Verfahren",
        "Cell biology techniques",
	],
	"Cohort studies": [	#label: RSDP2/5
        "Verhaltensdaten",
        "Citizen Science"
	],
	"Recordings": [	#label: RSDP2/6
        "Sensor based",
        "akustische Messung",
        "physiologische Messungen",
        "Elektrophysiologie",
        "Electrophysiology",
        "Messung mit Sensoren"
	],
	"Other": [	#label: RSDP2/other
        "Feldmessungen",        # interesting category for future surveys: "field studies"
        "Geländeerhebungen von Pflanzen und Tieren und statistische Analysen",
        "Fernerkundung",
        "Remote Sensing data",
        "Erhebungen im Gelände (Fallen, Vogelerfassung)",
        "Observation",
        "Fangmethoden",
        "Biological experiments",
        "experiments",
        "Messungen",
        "Statistische Analysen existierender Daten",
        "Messungen z.B. morphologischer Parameter",
        "Biosignale",
        "humane Bioproben",
        "Expositionsmodelle",
        "Hochdurchsatz-Verfahren",
        "Transkriptomanalyse",
        "biological samples",
        "Untersuchung von Versuchstieren",
        "online Erfassung von Prozessparameter",
        "Instrumentierung",
        "Messdaten der Maschine",
        "DAQ-Systeme",
        "Prozessparameter, mechanische Kenngrößen",
        "Data collected by the experiment and simulations produced based on models",
        "Direct Measurement",
        "measurements"
	]
}

RSDP2b_1_1 = {}
RSDP2b_1_2 = {}
RSDP2b_1_3 = {}
RSDP2b_2_1 = {}
RSDP2b_2_2 = {}
RSDP2b_2_3 = {}
RSDP2b_3_1 = {}
RSDP2b_3_2 = {}
RSDP2b_3_3 = {}
RSDP2b_4_1 = {}
RSDP2b_4_2 = {}
RSDP2b_4_3 = {}
RSDP2b_5_1 = {}
RSDP2b_5_2 = {}
RSDP2b_5_3 = {}
RSDP2b_6_1 = {}
RSDP2b_6_2 = {}
RSDP2b_6_3 = {}
RSDP2b_7_1 = {}
RSDP2b_7_2 = {}
RSDP2b_7_3 = {}

free_text_columns = {
    'PERBG1/other': PERBG1_other,
    'PERBG3/other': PERBG3_other,
    'PERBG3ING/other': PERBG3ING_other,
    'PERBG3GEO/other': PERBG3GEO_other,
    'PERBG3MATH/other': PERBG3MATH_other,
    'PERBG3PHYS/other': PERBG3PHYS_other,
    'PERBG3LIFE/other': PERBG3LIFE_other,
    'PERBG3BIO/other': PERBG3BIO_other,
    'PERBG3MED/other': PERBG3MED_other,
    'PERBG3PSYCH/other': PERBG3PSYCH_other,
    'PERBG3CHEM/other': PERBG3CHEM_other,
    'PERBG3AGRI/other': PERBG3AGRI_other,
    'PERBG6/other': PERBG6_other,
    'RSDP1c/other': RSDP1c_other,
    'RSDP3/other': RSDP3_other,
    'DTPUB3/other': DTPUB3_other,
    'DTPUB1b/other': DTPUB1b_other,
    'DTPUB4a/other': DTPUB4a_other,
    'DTPUB4b/other': DTPUB4b_other,
    'RDMPR1/other': RDMPR1_other,
    'RDMPR3/other': RDMPR3_other,
    'RDMPR7/other': RDMPR7_other,
    'DTPUB7/other': DTPUB7_other,
    'RDMPR6/other': RDMPR6_other,
    'RDMPR12/other': RDMPR12_other,
    'RDMPR11/other': RDMPR11_other,
    'SERVC1/other': SERVC1_other,
    'RDMPR10/1': RDMPR10,
    'RDMPR10/2': RDMPR10,
    'RDMPR10/3': RDMPR10,
    'DTPUB5/1': DTPUB5,
    'DTPUB5/2': DTPUB5,
    'DTPUB5/3': DTPUB5,
    'DTPUB5/4': DTPUB5,
    'DTPUB5/5': DTPUB5,
    #'SERVC3/_': SERVC3, #
    'RSDP2/other': RSDP2_other,
    'RSDP2b/1-1': RSDP2b_1_1,
    'RSDP2b/1-2': RSDP2b_1_2,
    'RSDP2b/1-3': RSDP2b_1_3,
    'RSDP2b/2-1': RSDP2b_2_1,
    'RSDP2b/2-2': RSDP2b_2_2,
    'RSDP2b/2-3': RSDP2b_2_3,
    'RSDP2b/3-1': RSDP2b_3_1,
    'RSDP2b/3-2': RSDP2b_3_2,
    'RSDP2b/3-3': RSDP2b_3_3,
    'RSDP2b/4-1': RSDP2b_4_1,
    'RSDP2b/4-2': RSDP2b_4_2,
    'RSDP2b/4-3': RSDP2b_4_3,
    'RSDP2b/5-1': RSDP2b_5_1,
    'RSDP2b/5-2': RSDP2b_5_2,
    'RSDP2b/5-3': RSDP2b_5_3,
    'RSDP2b/6-1': RSDP2b_6_1,
    'RSDP2b/6-2': RSDP2b_6_2,
    'RSDP2b/6-3': RSDP2b_6_3,
    'RSDP2b/7-1': RSDP2b_7_1,
    'RSDP2b/7-2': RSDP2b_7_2,
    'RSDP2b/7-3': RSDP2b_7_3
}

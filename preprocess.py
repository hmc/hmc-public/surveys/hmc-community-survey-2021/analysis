"""
This file is a file payload of the package `hifis_surveyval`.

Authors: HMC CCT1 TF Survey members
Last modified on 12.08.2022 
Preprocessing steps are defined in this file. 
"""

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.models.question_collection import QuestionCollection
from hifis_surveyval.models.question import Question
from hifis_surveyval.core.settings import Settings
from hifis_surveyval.core import util
from hifis_surveyval.models.answer_option import AnswerOption
from hifis_surveyval.models.mixins.yaml_constructable import YamlDict
from hifis_surveyval.models.translated import Translated

import matplotlib.pyplot as plt
from pathlib import Path
from pandas import DataFrame
import pandas as pd
import numpy as np
import os,sys
import csv
sys.path.append(os.getcwd())
from free_text_mapping import *
from scripts.constants import *


def preprocess(data: DataContainer) -> DataContainer:
    """
    Identification and removal of invalid responses are done during preprocessing.
    """
    # initialize Settings object, settings are read from hifis-surveyval.yml which
    # needs to exists in the root folder of the project, else a default settings object is created
    settings = Settings()
    # print all loaded question IDs
    all_ques_ids = []
    for question_coll in data.survey_questions:
        for ques_id in question_coll._questions.keys():
            all_ques_ids.append(question_coll._full_id+"/"+ques_id)
    print("Imported questions IDs:")
    print(all_ques_ids)
    
    #modify output path
    settings.ANALYSIS_OUTPUT_PATH = settings.ANALYSIS_OUTPUT_PATH.parent
    
    """Preprocess raw data."""
    dataframe : DataFrame = data.data_frame_for_ids(data.question_collection_ids)
    original_num_records : int = len(dataframe)
    
    ########## identify responses that have no content based on the filter interviewtime==0 #########
    no_content_response : Set = set()
    data_interviewtime: DataFrame = data.data_frame_for_ids(["interviewtime/_"])
    no_content_response.update(set(data_interviewtime[data_interviewtime["interviewtime/_"] == 0].index))
    no_content_response_num_records = len(no_content_response)
    
    # Remove invalid data
    data.mark_answers_invalid(no_content_response)
    data.remove_invalid_answer_sets()

    # Create output-path object
    output_dir_path = Path(settings.ANALYSIS_OUTPUT_PATH)
    
    #plot lastpage values
    #useful link: https://forums.limesurvey.org/forum/can-i-do-this-with-limesurvey/122072-solved-lastpage-response-value
    ques = "lastpage/_"
    categories: Question = data.question_for_id(ques)
    categories_df: DataFrame = DataFrame(categories.as_series())
    
    categories_freq: DataFrame = util.dataframe_value_counts(categories_df)
    categories_freq = categories_freq.rename({
            ques: categories.label
        }, axis="columns")
    print("==== Last page visitied - Frequencies ====")
    print(categories_freq)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.bar(categories_freq.index,categories_freq[ques])
    ax.set_title("Page in which the participants closed the survey")
    ax.set_xlabel("Question group")
    ax.set_ylabel("Count")
    ax.set_xticks(categories_freq.index.tolist())
    ax.set_xticklabels(categories_freq.index.tolist(), fontdict={'fontsize':6}, rotation=90)
    ax.set_yticks(np.arange(0, max(categories_freq[ques])+1, max(categories_freq[ques])/5))
    plt.tight_layout()
    fig.savefig(str(output_dir_path.joinpath("bar_plot_last_page.png")))
    plt.close()
    
    ########### remove incomplete entries ##########
    # identify incomplete records based on the absence of submitdate value
    incomplete_responses : Set = set()
    data_submitdate: DataFrame = data.data_frame_for_ids(["submitdate/_"])
    incomplete_responses.update(set(data_submitdate[data_submitdate["submitdate/_"].isnull()].index))
    incomplete_response_num_records = len(incomplete_responses)
    
    # Remove invalid data
    data.mark_answers_invalid(incomplete_responses)
    data.remove_invalid_answer_sets()
    
    # Valid records
    dataframe : DataFrame = data.data_frame_for_ids(data.question_collection_ids)
    completed_response_num_records = len(dataframe)
        
    
    #print values
    print("#############################################################")
    print("Original number of records : "+str(original_num_records))
    print("Number of empty responses : "+str(no_content_response_num_records))
    print("Number of incomplete responses : "+str(incomplete_response_num_records))
    print("Number of complete responses: "+str(completed_response_num_records))
    print("#############################################################")
    
    # generate plots
    # Total plot 
    labels = ['Complete','Incomplete','Empty']
    sizes = [completed_response_num_records,incomplete_response_num_records,no_content_response_num_records]
    color = ['#64FF56','#F77BFF','#8D9ABB']
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=90, colors=color)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.savefig(str(output_dir_path.joinpath("pie_chart_response_types.png")))
    plt.close()
        
    
    ## Generate unique list of values entered in free text fields ##used to perform mapping of free text responses into categories
    for q in free_text_columns.keys():
        df_sub: DataFrame = data.data_frame_for_ids([q])
        df_sub = df_sub.dropna(subset=[q])
        DataFrame(df_sub[q].unique()).to_csv(str(output_dir_path.joinpath("unique_values", q.replace("/","_")+".csv")), index=None)
    
    
    # # # # # # # # # # # # # # # # # # #
    # # # # # # # # # # # # # # # # # # #
    ###### APPLY FREE TEXT MAPPING ######
    # # # # # # # # # # # # # # # # # # #
    # # # # # # # # # # # # # # # # # # #
    
    # get all loaded question IDs
    all_ques_ids = []
    for question_coll in data.survey_questions:
        for ques_id in question_coll._questions.keys():
            all_ques_ids.append(question_coll._full_id+"/"+ques_id)
            
    # SC variables
    for qid in all_ques_ids:
        var = qid_variable_map[qid]
        #print(var)
        if var in sc_variables:
            if var=="researchArea": 
                continue #skip researchArea. They are dealt separately
            #check if "other" column exists
            other_colname = qid.split("/")[0]+("/other")
            if other_colname in free_text_columns.keys():
                #print(other_colname)
                ques = data.question_for_id(qid)
                ques_other = data.question_for_id(other_colname)
                ques_other_series = ques_other.as_series()
                
                #add missing answer options
                available_options = ques._answer_options
                mapping = free_text_columns[other_colname]
                for k,v in mapping.items():
                    if k not in available_options.keys():
                        new_answer = AnswerOption(parent_id=qid,
                                                  option_id=k,
                                                  text=Translated({'en-GB':k}),
                                                  label=k,
                                                  settings=data._settings,
                                                  value=k)
                        ques._add_answer_option(new_answer)
                
                #apply free text mapping 
                def replaceValues(val,mapping):
                    if val:
                        for k,v in mapping.items():
                            if str(val).lower().strip() in [str(v1).lower() for v1 in v]:
                                return None if k == "toRemove" else k
                        print("Unable to map: "+ val)
                        return val
                    else:
                        return None                
                for i in list(ques_other_series.index.values):
                    new_value = replaceValues(ques_other_series.loc[i], mapping)
                    ques_other.add_answer(i,new_value)
                    # store mapped value in the main variable column
                    if new_value is not None:
                        ques.add_answer(i,new_value)
         
    # MC variables
    for mc_var in mc_variables: #multiple choice questions
        #print(mc_var)
        # find list of questions for the given mc_var
        qid_list = [key for key,val in qid_variable_map.items() if mc_var in val]
        
        # apply free text mapping    
        for qid in qid_list:
            if qid in free_text_columns.keys():
                mapping = free_text_columns[qid]
                def replaceValues(val,map):
                    if val:
                        for k,v in map.items():
                            if str(val).lower().strip() in [str(v1).lower() for v1 in v]:
                                return None if k == "toRemove" else k
                        print("Unable to map: "+ val)
                        return val
                    else:
                        return None
                ques = data.question_for_id(qid)
                ques_series = ques.as_series()
                for i in list(ques_series.index.values):
                    new_value = replaceValues(ques_series.loc[i], mapping)
                    ques.add_answer(i,new_value)
            
    # MFT variables
    for mft_var in mft_variables:
        #print(mft_var)
        # find list of questions for the given mft_var
        qid_list = [key for key,val in qid_variable_map.items() if mft_var in val]
    
        # apply free text mapping    
        for qid in qid_list:
            if qid in free_text_columns.keys():
                mapping = free_text_columns[qid]
                def replaceValues(val,map):
                    if val:
                        for k,v in map.items():
                            if str(val).lower().strip() in [str(v1).lower() for v1 in v]:
                                return None if k == "toRemove" else k
                        print("Unable to map: "+ val)
                        return val
                    else:
                        return None
                ques = data.question_for_id(qid)
                ques_series = ques.as_series()
                for i in list(ques_series.index.values):
                    new_value = replaceValues(ques_series.loc[i], mapping)
                    ques.add_answer(i,new_value)
    
    #c leaning PERBG3 (all 3 levels)
    # create new question collection with three questions 
    yaml_dict: YamlDict = {
        "id": "PERBG03",
        "mandatory": False,
        "label": "PERBG03",
        "text": {
            "de-DE": "Research area in 3 levels",
            "en-GB": "Research area in 3 levels"
        },
        "questions": [
            {
                "id": "L1",
                "mandatory": False,
                "datatype": "str",
                "label": "PERBG03/L1",
                "text": {
                    "de-DE": "researchArea_L1",
                    "en-GB": "researchArea_L1"
                }
            },
            {
                "id": "L2",
                "mandatory": False,
                "datatype": "str",
                "label": "PERBG03/L2",
                "text": {
                    "de-DE": "researchArea_L2",
                    "en-GB": "researchArea_L2"
                }
            },
            {
                "id": "L3",
                "mandatory": False,
                "datatype": "str",
                "label": "PERBG03/L3",
                "text": {
                    "de-DE": "researchArea_L3",
                    "en-GB": "researchArea_L3"
                }
            }
        ]
    }
    # create and add QuestionCollection to the data
    new_collection = QuestionCollection._from_yaml_dictionary(yaml_dict, settings=data._settings)
    data._survey_questions["PERBG03"] = new_collection
    
    lc1_qid_list = ['PERBG3/_']
    lc2_qid_list = ["PERBG3ING/_","PERBG3GEO/_","PERBG3MATH/_","PERBG3PHYS/_",
                  "PERBG3LIFE/_","PERBG3PSYCH/_","PERBG3CHEM/_"]
    lc3_qid_list = ["PERBG3BIO/_","PERBG3MED/_","PERBG3AGRI/_"]
    
    null_vals = [None, np.nan]
    
    ques_L1 = data.question_for_id("PERBG03/L1")
    df_L1 = data.data_frame_for_ids(lc1_qid_list)
    for index, row in df_L1.iterrows():
        values = list(row.values)
        ans_val = next((item for item in values if item not in null_vals),np.nan)
        if ans_val is not np.nan:              
            ques_L1.add_answer(index, ans_val)
            
    ques_L2 = data.question_for_id("PERBG03/L2")
    df_L2 = data.data_frame_for_ids(lc2_qid_list)
    for index, row in df_L2.iterrows():
        values = list(row.values)
        ans_val = next((item for item in values if item not in null_vals),np.nan)
        if ans_val is not np.nan:              
            ques_L2.add_answer(index, ans_val)
            
    ques_L3 = data.question_for_id("PERBG03/L3")
    df_L3 = data.data_frame_for_ids(lc3_qid_list)
    for index, row in df_L3.iterrows():
        values = list(row.values)
        ans_val = next((item for item in values if item not in null_vals),np.nan)
        if ans_val is not np.nan:              
            ques_L3.add_answer(index, ans_val)
        
    for qid in lc1_qid_list: #lc3_qid_list+lc2_qid_list+lc1_qid_list: #only level 1 is currently cleaned #uncomment to clean all 3 levels
        other_qid = qid.split("/")[0]+("/other")
        mapping = free_text_columns[other_qid]
        def replaceValues(val,map):
            if val:
                for k,v in map.items():
                    if str(val).lower().strip() in [str(v1).lower() for v1 in v]:
                        return None if k == "toRemove" else k
                print("Unable to map: "+ str(val))
                return val
            else:
                return None
        ques_other = data.question_for_id(other_qid)
        ques_other_series = ques_other.as_series()
        for i in list(ques_other_series.index.values):
            new_value = replaceValues(ques_other_series.loc[i], mapping)
            if new_value is not None:
                new_value = new_value.split(":")
                ques_L1.add_answer(i,new_value[0])
                if len(new_value)>1:
                    ques_L2.add_answer(i,new_value[1])
                if len(new_value)>2:
                    ques_L3.add_answer(i,new_value[2])
        
    # Remove answers from institute directors and from respondants involved in management and coordination
    responses_to_remove : Set = set()
    data_perbg6: DataFrame = data.data_frame_for_ids(["PERBG6/_"])
    responses_to_remove.update(set(data_perbg6[data_perbg6["PERBG6/_"].isin(["Director of institute","Management and Coordination"])].index))
    data.mark_answers_invalid(responses_to_remove)
    data.remove_invalid_answer_sets()
    
    # fixing DTPUB answers
    # Reason: Few participants had chosen non-zero publication amount and then in a follow-up question answered "No publications made" in the "Other" field.
    print("Fixing DTPUB answers.....")
    responses_to_fix : Set = set()
    data_dtpub1b_other: DataFrame = data.data_frame_for_ids(["DTPUB1b/other"])
    responses_to_fix.update(set(data_dtpub1b_other[data_dtpub1b_other["DTPUB1b/other"]=="No data published"].index))
    data_dtpub3_other: DataFrame = data.data_frame_for_ids(["DTPUB3/other"])
    responses_to_fix.update(set(data_dtpub3_other[data_dtpub3_other["DTPUB3/other"]=="I have not yet published data"].index))
    data_dtpub4a_other: DataFrame = data.data_frame_for_ids(["DTPUB4a/other"])
    responses_to_fix.update(set(data_dtpub4a_other[data_dtpub4a_other["DTPUB4a/other"]=="No data published"].index))
    print("IDs to fix (total = "+str(len(responses_to_fix))+") : "+str(responses_to_fix))
    
    #set DTPUB6/1 to 0 and other DTPUB answers (except 4a and 4b) to None
    ques_dtpub6 = data.question_for_id("DTPUB6/1")
    dtpub_bool_qids = ["DTPUB1b/1","DTPUB1b/2","DTPUB1b/3",
                       "DTPUB3/1","DTPUB3/2","DTPUB3/3","DTPUB3/4","DTPUB3/5","DTPUB3/6","DTPUB3/7",
                       "DTPUB4a/0","DTPUB4b/0",
                       'DTPUB7/1','DTPUB7/21', 'DTPUB7/22','DTPUB7/23','DTPUB7/24',
                       'DTPUB7/31','DTPUB7/32','DTPUB7/33',
                       'DTPUB7/41','DTPUB7/42','DTPUB7/43','DTPUB7/44',
                       'DTPUB7/51','DTPUB7/52','DTPUB7/61','DTPUB7/62',
                       'DTPUB7/71','DTPUB7/72',
                       'DTPUB7/81','DTPUB7/82','DTPUB7/83',
                       'DTPUB7/91','DTPUB7/92','DTPUB7/93','DTPUB7/0']
    dtpub_str_qids = ["DTPUB1b/other","DTPUB3/other","DTPUB4a/other","DTPUB4b/other",                      
                      "DTPUB5/1","DTPUB5/2","DTPUB5/3","DTPUB5/4","DTPUB5/5",'DTPUB7/other']
    
    for i in dtpub_bool_qids:
        data.question_for_id(i).remove_answers(responses_to_fix)
    for i in dtpub_str_qids:
        data.question_for_id(i).remove_answers(responses_to_fix)
    for i in responses_to_fix:
        ques_dtpub6.add_answer(i,"0.0")
    
    # Move 4a to 4b and clear 4a
    dtpub4a_qids = ["DTPUB4a/1","DTPUB4a/2","DTPUB4a/3","DTPUB4a/4","DTPUB4a/5","DTPUB4a/6","DTPUB4a/7"]
    dtpub4b_qids = ["DTPUB4b/1","DTPUB4b/2","DTPUB4b/3","DTPUB4b/4","DTPUB4b/5","DTPUB4b/6","DTPUB4b/7"]
    dtpub4a_qns = []
    dtpub4b_qns = []
    dtpub4a_answers = []
    dtpub4b_answers = []
    for i in range(len(dtpub4a_qids)):
        dtpub4a_qns.append(data.question_for_id(dtpub4a_qids[i]))
        dtpub4b_qns.append(data.question_for_id(dtpub4b_qids[i]))
        dtpub4a_answers.append(data.question_for_id(dtpub4a_qids[i]).answers)
        dtpub4b_answers.append(data.question_for_id(dtpub4b_qids[i]).answers)
    for i in responses_to_fix:
        for j in range(len(dtpub4a_qids)):
            if dtpub4a_answers[j][i]:
                dtpub4b_qns[j].add_answer(i,"True")
    for j in dtpub4a_qns:
        j.remove_answers(responses_to_fix)
    
    # get the entire dataframe of responses
    dataframe = data.data_frame_for_ids(data.question_collection_ids).reset_index()  
    # Drop column SERVC3
    dataframe = dataframe.drop(columns=['SERVC3/_'])
    
    # Save data to file
    dataframe.to_csv(str(output_dir_path.joinpath("responses_cleaned_mapped_internal_use.csv")), index=None, quoting=csv.QUOTE_ALL)
    
    ###### Prepare dataset to publish #####
    
    # Anonymize values in column PERBG1/_ and /other
    dataframe["PERBG1/_"] = "Anonymized"
    dataframe["PERBG1/other"] = "Anonymized"
    
    # Drop PERBG3 columns (there are 3 new columns for 3 levels of research area)
    # col_list = [i for i in dataframe.columns if "PERBG3" in i]
    # dataframe = dataframe.drop(col_list,axis= 1)
    
    # Combine RSDP1c answers into one column with True or empty string
    # col_list = [i for i in dataframe.columns if "RSDP1c/" in i]
    # dataframe['RSDP1c/_'] = dataframe[col_list].apply(
    #     lambda x: True if len(''.join(x.dropna().astype(str)))>0 else "",
    #     axis=1)
    # dataframe = dataframe.drop(col_list, axis=1)
    
    # Drop the three new research area columns
    col_list = [i for i in dataframe.columns if "PERBG03" in i]
    dataframe = dataframe.drop(col_list,axis= 1)
    
    # Anonymize entries in RSDP1c_other by overwriting values by True
    dataframe["RSDP1c/other"] = dataframe["RSDP1c/other"].apply(
                lambda x: "" if pd.isna(x) else "Anonymized" )
    
    # Software
    col_list = [i for i in dataframe.columns if "RDMPR10/" in i]
    software_counts = pd.melt(dataframe[col_list])["value"].value_counts()
    software_to_anonymize = list(software_counts[software_counts<4].index)
    software_to_anonymize += ['Other','Own software','SPEC','TAU']
    for col in col_list:
        dataframe[col] = dataframe[col].apply(
                lambda x: "Anonymized" if x in software_to_anonymize else x)
    
    # Repositories
    col_list = [i for i in dataframe.columns if "DTPUB5/" in i]
    repo_to_anonymize = ["RODARE", "Institutional server", "HALO database", "GSI repository", "FZJ Datapub",
                         "GEOFON", "EOC Geoservice", "HZB ICAT", "ROBIS", "EOC IMPC", "e!DAL", "GFZ data services",
                         "Helmholtz Coastal Data Center", "EOC DIMS", "TORE", "ILDG", "KITopen"]
    for col in col_list:
        dataframe[col] = dataframe[col].apply(
                lambda x: "Institutional Repository" if x in repo_to_anonymize else x)
        
    # Save data to file
    dataframe.to_csv(str(output_dir_path.joinpath("responses_cleaned_mapped_to_publish.csv")), index=None, quoting=csv.QUOTE_ALL)
            
    return data #DataContainer object
import json
import yaml
import os
import pandas as pd
from rdflib import URIRef, Literal, BNode, Graph, Namespace
from rdflib.namespace import DCTERMS, SKOS, DCAT, FOAF, ORG, PROV, RDF, RDFS, XSD, CSVW
from rdflib.plugins.sparql import prepareQuery
from datetime import datetime


# >>> CONFIG ===


## the license that is used for our publication
LICENSE_URL = "https://spdx.org/licenses/CC-BY-4.0.html"
## the namespace of the generated document
DOCUMENT_NS = "https://helmholtz-metadata.de/survey#" # ensure trailing slash or hashtag!
## the link to the yaml-question definitions
YAML_DEFINITION_LINK = "https://codebase.helmholtz.cloud/hmc/hmc-public/surveys/hmc-community-survey-2021/analysis/-/blob/main/metadata/" # ensure trailing slash!
## DOI of the data publication
DATA_DOI = "10.7802/2433"
REPORT_DOI = "10.3289/HMC_publ_05"
## the link to the survey result CSV
RESULT_CSV_LINK = f"https://doi.org/{DATA_DOI}"
## URL to the public HMC Gitlab repository of the survey
HMC_REPO_URL="https://codebase.helmholtz.cloud/hmc/hmc-public/surveys/hmc-community-survey-2021"


# <<< CONFIG ===

# === GLOBAL VARS ===

# setup paths to required files
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
METADATA_DIR_PATH = os.path.join(SCRIPT_DIR, "../metadata")
CSV_PATH = os.path.join(SCRIPT_DIR, "../output/responses_cleaned_mapped_to_publish.csv")
OUT_PATH = os.path.join(SCRIPT_DIR, "..")
METADATA_STUB_PATH = os.path.join(SCRIPT_DIR, "metadata_stub.json")

# add missing namespaces DISCO, ADMS
ADMS = Namespace("http://www.w3.org/ns/adms#")
DISCO = Namespace("http://rdf-vocabulary.ddialliance.org/discovery#")
I = Namespace(DOCUMENT_NS)

## setup graph
G = Graph()
## graph bindings
G.bind("dcterm", DCTERMS)
G.bind("skos", SKOS)
G.bind("dcat", DCAT)
G.bind("foaf", FOAF)
G.bind("org", ORG)
G.bind("prov", PROV)
G.bind("rdf", RDF)
G.bind("adms", ADMS)
G.bind("disco", DISCO)
G.bind("hmc", I)
G.bind("csvw", CSVW)

## survey's local identifiers
URI_SURVEY = I.survey
URI_UNIVERSE = I.universe
URI_QUESTIONNAIRE = I.questionnaire
URI_DATASET = I.dataset_1
URI_CSVW_TABLE = I.csvw_table
URI_CSVW_SCHEMA = I.csvw_schema
URI_INQUIRY_PERIOD = I.inquiryPeriod

# === END GLOBAL VARS ===

# === FN DEFINITIONS ====

def link_to_yaml_files(name: str) -> str: # link must be adapted to correct location
    file_name = f"{name}.yml"
    return YAML_DEFINITION_LINK + file_name

def create_DISCO_preamble_metadata() -> None:
    """ Adds static metadata (preamble metadata) from the metadata stub to the global graph
    """
    # load preamble metadata from json file (metadata_stub)
    metadata_stub = {}
    with open(METADATA_STUB_PATH, 'r') as f:
        metadata_stub = json.load(f)
    # add title, identifiers (DOI), inquiryPeriod, creator aka HMC
    G.add((URI_SURVEY, RDF.type, DISCO.Study))
    G.add((URI_SURVEY, DCTERMS.title, Literal(metadata_stub["name"], lang="en")))
    G.add((URI_SURVEY, DCTERMS.alternative, Literal("A survey on research data management practices among researchers in the Helmholtz Association", lang="en")))
    doi = BNode()
    G.add((URI_SURVEY, DCTERMS.identifier, doi))
    G.add((doi, RDF.type, ADMS.Identifier))
    G.add((doi, SKOS.notation, Literal(DATA_DOI)))
    G.add((doi, ADMS.schemeAgency, Literal("DOI")))
    G.add((URI_SURVEY, RDFS.seeAlso, URIRef(f"https://doi.org/{DATA_DOI}")))
    G.add((URI_SURVEY, RDFS.seeAlso, URIRef(HMC_REPO_URL)))
    G.add((URI_SURVEY, DCTERMS.temporal, URI_INQUIRY_PERIOD))
    G.add((URI_INQUIRY_PERIOD, RDF.type, DCTERMS.PeriodOfTime))
    G.add((URI_INQUIRY_PERIOD, DISCO.startDate, Literal(metadata_stub["inquiryPeriod"]["start"], datatype=XSD.date)))
    G.add((URI_INQUIRY_PERIOD, DISCO.endDate, Literal(metadata_stub["inquiryPeriod"]["end"], datatype=XSD.date)))
    hmc = BNode()
    G.add((URI_SURVEY, DCTERMS.creator, hmc))
    G.add((hmc, RDFS.label, Literal(metadata_stub["institution"]["name"])))
    G.add((hmc, SKOS.notation, Literal(metadata_stub["institution"]["acronym"])))
    helmholtz = BNode()
    G.add((hmc, ORG.memberOf, helmholtz))
    G.add((helmholtz, RDFS.label, Literal("Helmholtz Association of German Research Centres")))
    now = datetime.now().replace(microsecond=0)
    G.add((URI_SURVEY, DCTERMS.created, Literal(now.astimezone().isoformat(), datatype=XSD.dateTime)))

    G.add((URIRef(f"https://doi.org/{REPORT_DOI}"), RDF.type, FOAF.Document))
    G.add((URIRef(f"https://doi.org/{REPORT_DOI}"), FOAF.primaryTopic, URI_SURVEY))
    G.add((URIRef(f"https://doi.org/{REPORT_DOI}"), DCTERMS.title, Literal("A survey on research data management practices among researchers in the Helmholtz Association", lang="en")))
    G.add((URI_SURVEY, DCTERMS.relation, URIRef(f"https://doi.org/{REPORT_DOI}")))

    # add contributors
    for person in metadata_stub["contributors"]:
        p = BNode()
        G.add((URI_SURVEY, DCTERMS.contributor, p))
        G.add((p, RDF.type, FOAF.Person))
        G.add((p, FOAF.name, Literal(person["name"])))
        orcid = BNode()
        G.add((p, ADMS.identifier, orcid))
        G.add((orcid, RDF.type, ADMS.Identifier))
        G.add((orcid, SKOS.notation, Literal(person["identifier"]["identifierValue"])))
        G.add((orcid, ADMS.schemeAgency, Literal(person["identifier"]["identifierType"])))

    # add the universe to the survey (the universe is the answer to: who did we ask?)
    G.add((URI_UNIVERSE, RDF.type, DISCO.Universe))
    G.add((URI_UNIVERSE, SKOS.definition, Literal("Researchers in the Helmholtz Association of German Research Centres.", lang="en")))
    G.add((URI_SURVEY, DISCO.universe, URI_UNIVERSE))
    # add the questionnaire
    G.add((URI_QUESTIONNAIRE, RDF.type, DISCO.Questionnaire))
    G.add((URI_SURVEY, DISCO.instrument, URI_QUESTIONNAIRE))
    # add the dataset, more information of the dataset is added in step 4
    G.add((URI_DATASET, RDF.type, DISCO.LogicalDataset))
    G.add((URI_SURVEY, DISCO.product, URI_DATASET))


def map_datatype_to_xsd(datatype: str) -> URIRef:
    """ Map question metadata's datatypes to XSD
    """
    if datatype == "bool":
        return XSD.boolean
    elif datatype == "str":
        return XSD.string
    elif datatype == "float":
        return XSD.float
    elif datatype == "int":
        return XSD.int
    else:
        return XSD.string


def read_questions_from_file() -> dict:
    """ Read question "metadata" from file and translate to RDF
    """
    question_metadata = []
    for filename in os.listdir(METADATA_DIR_PATH):
        with open(os.path.join(METADATA_DIR_PATH, filename), 'r') as f:
            q_meta = yaml.safe_load(f)
            if "details" in q_meta and "ignore" in q_meta["details"] and q_meta["details"]["ignore"] == True:
                continue
            question_metadata.append(q_meta)
    return question_metadata


def create_variable_from_subquestion(sub_question: dict, graph) -> URIRef:
    """ Creates a variable for multi/single choice questions
        Notice: datatype representation ist not created!
        Notice: variable is also not added to question, needs to be done by caller
    """
    lbl = sub_question['label'].replace("/", "_")
    var = I[f"var_{lbl}"]
    graph.add((var, RDF.type, DISCO.Variable))
    graph.add((var, SKOS.notation, Literal(sub_question['label'])))
    graph.add((URI_DATASET, DISCO.variable, var))
    for lang in sub_question['text'].keys():
        graph.add((var, SKOS.prefLabel, Literal(sub_question['text'][lang], lang=lang)))
    return var


def create_concept_scheme_representation_from_single_choice_answer(qid: str, sqid: str, answers: list, graph) -> URIRef:
    """ Creates concept and concept schemes for single choice questions
        Hint: qid = question id; sqid = sub question id
    """
    c_scheme = I[f"c_scheme_{qid}_{sqid}"]
    graph.add((c_scheme, RDF.type, SKOS.ConceptScheme))
    graph.add((c_scheme, RDF.type, DISCO.Representation))
    for answer in answers:
        concept = I[f"c_{qid}_{sqid}_{answer['id']}"]
        graph.add((concept, RDF.type, SKOS.Concept))
        graph.add((concept, SKOS.notation, Literal(answer["label"])))
        for lang in answer['text'].keys():
            graph.add((concept, SKOS.prefLabel, Literal(answer['text'][lang], lang=lang)))
        graph.add((concept, SKOS.inScheme, c_scheme))
        graph.add((c_scheme, SKOS.hasTopConcept, concept))
    return c_scheme


def question_to_RDF(question: dict, graph) -> None:
    """ Create DISCO question, variables and concepts from question
    """
    q = I[f"q_{question['id']}"]
    question["uriref"] = q
    graph.add((q, RDF.type, DISCO.Question))
    seeAlso = BNode()
    graph.add((seeAlso, DCTERMS.format, URIRef("https://www.nationalarchives.gov.uk/PRONOM/fmt/818")))
    graph.add((seeAlso, RDF.value, URIRef(link_to_yaml_files(question["id"]))))
    graph.add((q, RDFS.seeAlso, seeAlso))
    graph.add((q, SKOS.note, Literal(f"Question format/style: {question['response_format']}", lang="en")))
    for lang in question["text"].keys():
        graph.add((q, DISCO.questionText, Literal(question["text"][lang], lang=lang)))
    if question["response_format"] in ("multiple choice", "free text", "none"): # determine questions response format
        for sub_question in question["questions"]:
            var = create_variable_from_subquestion(sub_question, graph)
            graph.add((var, DISCO.question, q))
            graph.add((var, DISCO.representation, map_datatype_to_xsd(sub_question['datatype'])))
    elif question["response_format"] in ("array", "single choice", "slider"):
        for sub_question in question["questions"]:
            var = create_variable_from_subquestion(sub_question, graph)
            graph.add((var, DISCO.question, q))
            if "answers" in sub_question:
                rep = create_concept_scheme_representation_from_single_choice_answer(question['id'], sub_question['id'], sub_question["answers"], graph)
                graph.add((var, DISCO.representation, rep))
            else:
                graph.add((var, DISCO.representation, map_datatype_to_xsd(sub_question['datatype'])))
    else:
        print(f"[Warning]: This question was not processed: {question['id']}")
    if "details" in question and "anonymizationStrategy" in question["details"]:
        graph.add((q, SKOS.editorialNote, Literal("Answers of this questions have partial or fully be anonymized because of data protection. 'anonymized' means there are free-text answers which were replaced with the string \"Anonymized\". 'rephrased' means that answers were sorted into an existing categories (if applicable) or new categories were created (if no categories exists). More information can be found in the associated report (see Chapter \"Survey Design and Analysis\" -> \"Data analysis\").", lang="en")))

def setup_CSVW() -> None:
    """ Setup the CSVW table description, does a header exists and delimiter symbol
    """
    G.add((URI_CSVW_TABLE, RDF.type, CSVW.Table))
    dialect = BNode() # States that CSV has header column and uses ',' as value delimiter
    G.add((URI_CSVW_TABLE, CSVW.dialect, dialect))
    G.add((dialect, RDF.type, CSVW.Dialect))
    G.add((dialect, CSVW.delimiter, Literal(",")))
    G.add((dialect, CSVW.header, Literal("true", datatype=XSD.boolean)))
    G.add((URI_CSVW_TABLE, CSVW.tableSchema, URI_CSVW_SCHEMA))
    G.add((URI_CSVW_SCHEMA, RDF.type, CSVW.Schema))
    G.add((URI_CSVW_TABLE, CSVW.url, URIRef(RESULT_CSV_LINK)))

# === END FN DEFINITIONS ====

# === PROCESSING ===

# 1. Add preamble metadata
create_DISCO_preamble_metadata()

# 2. Add questions
for question in read_questions_from_file():
    question_to_RDF(question, G) # set a additional field 'uriref'
    G.add((URI_QUESTIONNAIRE, DISCO.question, question["uriref"]))

# 3. Setup CSVW
setup_CSVW()

list_ref_curr = BNode() # used to list the columns, points to the current column
list_ref_next = BNode() # used to list the columns, points to the rest of the list
G.add((URI_CSVW_SCHEMA, CSVW.column, list_ref_curr))

idCol = BNode()
G.add((idCol, CSVW.name, Literal("id")))
G.add((idCol, CSVW.datatype, XSD.int))
G.add((idCol, DCTERMS.description, Literal("Ignore this column, for internal use only.", lang="en")))
G.add((list_ref_curr, RDF.first, idCol))
G.add((list_ref_curr, RDF.rest, list_ref_next))
list_ref_curr = list_ref_next
list_ref_next = BNode()

## read CSV file for column names and create CSVW description
response_data = pd.read_csv(CSV_PATH, sep=",")
## process columns
col_len = len(response_data.columns)
for col_idx in range(1, col_len): # skip the "id" column
    # get column name and corresponding variable name
    col_name = response_data.columns[col_idx]
    var_name = f"var_{col_name.replace('/', '_')}"
    if not (I[var_name], RDF.type, DISCO.Variable) in G:
        print(f"Could not process CSV column: {col_name}.")
        continue
    # ask graph if variable represenation is a disco:Represenation
    qIsRepresenation = prepareQuery("ASK WHERE { ?variable disco:representation ?rep . ?rep rdf:type disco:Representation . }", initNs={"rdf": RDF, "disco": DISCO, "xsd": XSD})
    result = G.query(qIsRepresenation, initBindings={"variable": I[var_name]})

    col = BNode() # prepare column
    G.add((col, CSVW.name, Literal(col_name)))
    if bool(result) == True: # variable is represented by a Representation
        G.add((col, CSVW.datatype, XSD.string))
    else:                    # variable is a simple type
        G.add((col, CSVW.datatype, G.value(I[var_name], DISCO.representation)))
    G.add((col, RDFS.seeAlso, I[var_name])) # simply link to variable
    G.add((list_ref_curr, RDF.first, col))
    if col_idx + 1 < col_len:
        G.add((list_ref_curr, RDF.rest, list_ref_next))
        list_ref_curr = list_ref_next
        list_ref_next = BNode()
    else:
        G.add((list_ref_curr, RDF.rest, RDF.nil))

# 4. Create disco:DataFile to link result file, and expand metadata of the disco:LogicalDataSet
## rights
G.add((URI_DATASET, DCTERMS.license, URIRef(LICENSE_URL)))
G.add((URI_DATASET, DCTERMS.temporal, URI_INQUIRY_PERIOD))
G.add((URI_DATASET, DISCO.isPublic, Literal("true", datatype=XSD.boolean)))
G.add((URI_DATASET, DISCO.variableQuantity, Literal(col_len-1, datatype=XSD.nonNegativeInteger))) # minus id column
G.add((URI_DATASET, DCTERMS.title, Literal("responses_cleaned_mapped_to_publish.csv")))
G.add((URI_DATASET, DISCO.universe, URI_UNIVERSE))

dataFile = BNode()
G.add((dataFile, RDF.type, DISCO.DataFile))
G.add((dataFile, DCTERMS.temporal, URI_INQUIRY_PERIOD))
G.add((dataFile, DISCO.variableQuantity, Literal(col_len-1, datatype=XSD.nonNegativeInteger)))    # minus id column
G.add((dataFile, DISCO.caseQuantity, Literal(len(response_data), datatype=XSD.nonNegativeInteger)))
G.add((dataFile, DCTERMS.format, Literal("text/csv")))
G.add((dataFile, RDFS.seeAlso, URI_CSVW_TABLE))

G.add((URI_DATASET, DISCO.dataFile, dataFile))

# === END PROCESSING ===

# === SERIALIZE ===
G.serialize(os.path.join(OUT_PATH, "metadata-rdf.ttl"), "turtle", None, "utf-8")
G.serialize(os.path.join(OUT_PATH, "metadata-rdf.json"), "json-ld", None, "utf-8")
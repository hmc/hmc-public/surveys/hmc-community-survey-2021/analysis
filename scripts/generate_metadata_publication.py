import json
import yaml
import os
import hashlib
import pandas as pd
from jsonschema import validate, ValidationError

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
METADATA_DIR_PATH = os.path.join(SCRIPT_DIR, "../metadata")
SCHEMA_PATH = os.path.join(SCRIPT_DIR, "../schemas/survey.schema.json")
CSV_PATH = os.path.join(SCRIPT_DIR, "../output/responses_cleaned_mapped_to_publish.csv")
OUT_FILE_PATH = os.path.join(SCRIPT_DIR, "../metadata_export.json")
METADATA_STUB_PATH = os.path.join(SCRIPT_DIR, "metadata_stub.json")


def getSHA256(path: str):
    with open(path,"rb") as f:
        bytes = f.read() # read entire file as bytes
    return hashlib.sha256(bytes).hexdigest()



question_metadata = []
schema = None

for filename in os.listdir(METADATA_DIR_PATH):
    with open(os.path.join(METADATA_DIR_PATH, filename), 'r') as f:
        q_meta = yaml.safe_load(f)
        if "details" in q_meta and "ignore" in q_meta["details"] and q_meta["details"]["ignore"] == True:
            continue
        question_metadata.append(q_meta)

with open(SCHEMA_PATH, 'r') as schema_f:
    schema = json.load(schema_f)

if len(question_metadata) == 0 or schema is None:
    print("[Error]: Cannot find metadata or schema is missing. Abort.")
    exit(-1)


metadata_document = {}
with open(METADATA_STUB_PATH, 'r') as f:
    metadata_document = json.load(f)
metadata_document["questions"] = question_metadata
metadata_document["data"]["sha256checksum"] = getSHA256(CSV_PATH)

response_data = pd.read_csv(CSV_PATH, sep=",")
response_data_cols = response_data.columns

for q in question_metadata:
    matching_cols_in_response_data = []
    index = 0
    for index in range(len(response_data.columns)):
        # find columns belonging to question ('q')
        if str(response_data_cols[index]).startswith(str(q['id'] + "/")):
            matching_cols_in_response_data.append(index)
    # write data description for question
    metadata_document['data']["columns"].append({
        "columnIndices": matching_cols_in_response_data,
        "question": q['id']
    })

try:
    validate(instance=metadata_document, schema=schema)
except ValidationError as e:
    print("Metadata not valid gainst schema.", e.message)
    exit(1)

for assoc in metadata_document['data']["columns"]:
    if len(assoc['columnIndices']) == 0:
        print(f"[Warning] No column indecies for question: {assoc['question']}")

# write metadata to disk
with open(OUT_FILE_PATH, "w") as out_fp:
    json.dump(metadata_document, out_fp, ensure_ascii=False, indent=2)

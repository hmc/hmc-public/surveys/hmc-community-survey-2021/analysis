"""
This file is a file payload of the package `hifis_surveyval`.

Authors: HMC CCT1 TF Survey members
Last modified on 12.08.2022 
This is the main script where the analysis execution begins. 
Variables and functions defined in other files are used here.

The DataContainer object provided by hifis_surveyval is converted 
to a dataframe and plots are generated. 

"""

#import necessary libraries

from pathlib import Path
from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
import pandas as pd
import csv

from constants import *
import plotting_functions


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """Execute main script."""
    
    # get all loaded question IDs
    all_ques_ids = []
    for question_coll in data.survey_questions:
        for ques_id in question_coll._questions.keys():
            all_ques_ids.append(question_coll._full_id+"/"+ques_id)
    
    # get the entire dataframe of responses
    dataframe = data.data_frame_for_ids(data.question_collection_ids).reset_index()
    
    # create output path object
    output_dir_path = Path(hifis_surveyval.settings.ANALYSIS_OUTPUT_PATH)
        
    # # # # # # # # # # # # # #
    # # # # # # # # # # # # # # 
    ##### GENERATE PLOTS ######
    # # # # # # # # # # # # # #   
    # # # # # # # # # # # # # #
    
    #plots are stored in PNG and SVG formats in the output folder of hifis surveyval
    
    #list to store metadata of images
    fig_logs = []
    
    #single choice questions
    for var in sc_variables:
        qid = [key for key,val in qid_variable_map.items() if var in val][0]
        order = False if var in ["pubAmount","dataAmount_lsf","dataGatherTime"] else True
        #Hub-wise plot
        fig_log_dict = {"filename":variable_plot_filenames_by_hub.get(var),
                             "title":variable_titles.get(var)}
        fig_log_dict.update(plotting_functions.scCountPlotSingleHubs(data = dataframe, 
                            qid = qid, 
                            normalize = True, 
                            path = str(output_dir_path.joinpath(variable_plot_filenames_by_hub.get(var))),
                            outputFormats = [".png",".svg"]
                            ))
        fig_logs = fig_logs + [fig_log_dict]
        
        #Total plot
        orientation = "h"
        barlabels = True
        figsizex = 10
        figsizey = 5
        
        #vertical bars and no barlabels for pubAmount plot
        if qid=="DTPUB6/1":
            orientation = "v"
            barlabels = False
            
        fig_log_dict = {"filename":variable_plot_filenames_total.get(var),
                             "title":variable_titles.get(var)}
        fig_log_dict.update(plotting_functions.scEDAcountPlot(data = dataframe, 
                            qid = qid, 
                            orientation = orientation,
                            figsizex = figsizex, 
                            figsizey = figsizey,
                            barlabels = barlabels,
                            sortByCount = order,
                            normalize = True, 
                            path = str(output_dir_path.joinpath(variable_plot_filenames_total.get(var))),
                            outputFormats = [".png",".svg"]
                            ))
        fig_logs = fig_logs + [fig_log_dict]
            
        
    #List of unique values for PERBG1
    institutes_hubs = pd.DataFrame(dataframe[["PERBG1/_","PERBG2/_"]].groupby(["PERBG1/_","PERBG2/_"]).size().unstack())
    institutes_hubs.to_csv(str(output_dir_path.joinpath("Participating_institutes.csv")), 
        quoting=csv.QUOTE_ALL)
    
    
    #Piechart for PERBG2
    fig_log_dict =  {"filename":"HCS_EDA_Total_researchFieldHGF_piechart",
                         "title":"Distribution of responses among hubs"}
    fig_log_dict.update(plotting_functions.scPieChart(data = dataframe, 
                            qid = "PERBG2/_",
                            path = str(output_dir_path.joinpath("HCS_EDA_Total_researchFieldHGF_piechart")),
                            outputFormats = [".png",".svg"]
                            ))
    fig_logs = fig_logs + [fig_log_dict]
    
    
    #multiple choice questions
    for mc_var in mc_variables: #multiple choice questions
        qid_list = [key for key,val in qid_variable_map.items() if mc_var in val]
        if len(qid_list)>1 and set(qid_list).issubset(all_ques_ids):
            catByVar = True if mc_var in mc_var_group_plot else False
            #Hub-wise plot
            fig_log_dict = {"filename":variable_plot_filenames_by_hub.get(mc_var),
                                 "title":variable_titles.get(mc_var)}
            fig_log_dict.update(plotting_functions.mcCountPlotSingleHubs(data = dataframe, 
                            mcvar = mc_var, 
                            orientation = "h", 
                            catByVar = catByVar, 
                            normalize = True,
                            path = str(output_dir_path.joinpath(variable_plot_filenames_by_hub.get(mc_var))),
                            outputFormats = [".png",".svg"]
                            ))
            fig_logs = fig_logs + [fig_log_dict]
            
            #Total plot
            figsizex = 10
            figsizey = 5
            increase_y_vars = ["pubMetadata","docAuto","docDigital","servFormat","dataFormats"]
            if mc_var in increase_y_vars :
                figsizey = 10
            fig_log_dict = {"filename":variable_plot_filenames_total.get(mc_var),
                                 "title":variable_titles.get(mc_var)}
            fig_log_dict.update(plotting_functions.mcEDAcountPlot(data = dataframe, 
                            figsizex = figsizex, figsizey = figsizey,
                            mcvar = mc_var, 
                            catByVar = catByVar, 
                            normalize = True,
                            path = str(output_dir_path.joinpath(variable_plot_filenames_total.get(mc_var))),
                            outputFormats = [".png",".svg"]
                            ))
            fig_logs = fig_logs + [fig_log_dict]
            
    #multiple free-text answer questions
    for mft_var in mft_variables: #multiple free-text answer questions
        # find list of questions for the given mft_var
        qid_list = [key for key,val in qid_variable_map.items() if mft_var in val]
    
        if len(qid_list)>1 and set(qid_list).issubset(all_ques_ids):
            #Hub-wise plot
            fig_log_dict = {"filename":variable_plot_filenames_by_hub.get(mft_var),
                                 "title":variable_titles.get(mft_var)}
            fig_log_dict.update(plotting_functions.mcCountPlotSingleHubs(data = dataframe, 
                            mcvar = mft_var, 
                            orientation = "h",
                            normalize=True,
                            path = str(output_dir_path.joinpath(variable_plot_filenames_by_hub.get(mft_var))),
                            outputFormats = [".png",".svg"]
                            ))
            fig_logs = fig_logs + [fig_log_dict]
            
            #Total plot
            fig_log_dict = {"filename":variable_plot_filenames_total.get(mft_var),
                                 "title":variable_titles.get(mft_var)}
            fig_log_dict.update(plotting_functions.mcEDAcountPlot(data = dataframe, 
                            mcvar = mft_var,
                            normalize = True,
                            path = str(output_dir_path.joinpath(variable_plot_filenames_total.get(mft_var))),
                            outputFormats = [".png",".svg"]
                            ))
            fig_logs = fig_logs + [fig_log_dict]
    

    #upset plots
    for mc_var in mc_variables: #multiple choice questions
     qid_list = [key for key,val in qid_variable_map.items() if mc_var in val]
     if len(qid_list)>1 and set(qid_list).issubset(all_ques_ids):
         catByVar = True if mc_var in mc_var_group_plot else False
         fig_log_dict = {"filename":variable_plot_filenames_upset.get(mc_var),
                              "title":variable_titles.get(mc_var)}
         fig_log_dict.update(plotting_functions.mcUpSetPlot(data = dataframe, 
                            mcvar = mc_var, 
                            catByVar = catByVar, 
                            normalize = True,
                            path = str(output_dir_path.joinpath(variable_plot_filenames_upset.get(mc_var))),
                            outputFormats = [".png",".svg"]
                            ))
         fig_logs = fig_logs + [fig_log_dict]         
    
    #save metadata of images to file
    fig_logs = pd.DataFrame(fig_logs)
    fig_logs.to_csv(str(output_dir_path.joinpath("figures_metadata.csv")), index=None, quoting=csv.QUOTE_ALL)

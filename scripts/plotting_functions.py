#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Authors: HMC CCT1 TF Survey members
Last modified on 12.08.2022 
Plotting fucntions are defined in this file
"""

import matplotlib.pyplot as plt
from constants import *
import pandas as pd
import upsetplot as up

# set common styling for plots
plt.rcParams["font.family"] = "Arial" #"serif"
plt.rcParams["font.size"] = 14
plt.rcParams["axes.titlesize"] = 20
plt.rcParams["axes.titleweight"] = "bold"

#####################################################
###### DEFINE FUNCTION TO ITERATE THROUGH HUBS ######
#################### COUNTPLOTS #####################
#####################################################

def scCountPlotSingleHubs(data, qid, sortByCount = False,
                          figsizex = 15, figsizey = 20, orientation = "h", 
                          barlabels = True, normalize = False, equalXaxis = True,
                          title = "", path = None, outputFormats = [".png"]):
    
    """Create a figure with six subplots (countplots) according to HMC Hubs.
    Hub plots will be displayed in the official Hub color.
    Optional: Save figure to path.
    
    Args:
        data: pd.DataFrame() 
            The dataframe to be analyzed.
        qid: str
            Name of column in the dataframe to be plotted.
        sortByCount: bool, default False
            Flag indicating whether the bars are to be sorted by count
        figsizex: int, default 15
            x-value of final figure size.
        figsizey: int, default 20
            y-value of final figure size.
        orientation: ["v", "h"], default "h"
            "v" -> vertical bars
            "h" -> horizontal bars
        barlabels: bool, default True
            When True, labels are added to each bar and axis ticks are removed
            When False, axis ticks are included and bars have no labels
        normalize: bool, default False
            Indicates whether the counts are to be normalized
        equalXaxis: bool, default True
            Flag indicating whether the x-axis limits for all subplots are to be equal
        title: str, default ""
            Title of Figure.
        path: str, default None
            File path to save figure.
        outputFormats: list, default [".png"]
            list of output formats in which the plot is to be exported.
        
    Returns:
        dict {"n":<length of plotted data>}
    """
    if orientation == "v":
        kind = "bar"
        xlabel = ""
        ylabel = "percentage" if normalize else "count"
    elif orientation == "h":
        kind = "barh"
        xlabel = "percentage" if normalize else "count"
        ylabel = ""
    else:
        print("orientation = "+str(orientation)+"\nUnknown orientation value provided. Orientation needs to be either \"v\" or \"h\"")

    fontsize = 18
    
    fig = plt.figure(figsize = (figsizex,figsizey))

    ax1 = fig.add_subplot(321)
    ax2 = fig.add_subplot(322)
    ax3 = fig.add_subplot(323)
    ax4 = fig.add_subplot(324)
    ax5 = fig.add_subplot(325)
    ax6 = fig.add_subplot(326)
    
    fig_other = plt.figure(figsize = (figsizex,figsizey))
    
    ax1_other = fig_other.add_subplot(321)
    ax2_other = fig_other.add_subplot(322)
    ax3_other = fig_other.add_subplot(323)
    ax4_other = fig_other.add_subplot(324)
    ax5_other = fig_other.add_subplot(325)
    ax6_other = fig_other.add_subplot(326)
    
    #axes are defined in the same order as answer options
    hubaxes = {
        'AST':[ax5,ax5_other],
        'E&E':[ax2,ax2_other],
        'Energy':[ax1,ax1_other],
        'Health':[ax3,ax3_other],
        'Info':[ax4,ax4_other],
        'Matter':[ax6,ax6_other]
    }
    
    axMax = 0
    
    #get the question id of the question on hubs
    qid_hub = [key for key,val in qid_variable_map.items() if val == "researchFieldHGF"][0]
    
    valid_plot = False
    valid_plot_other = False
    
    for hubname,hubabbr in hubdict.items():
        plotdata = data.loc[data[qid_hub] == hubname][qid].value_counts(normalize=normalize).sort_index()
        num_resp = data.loc[data[qid_hub] == hubname][qid].count()
        
        # separate newly defined categories and add count to "other"
        ref_list = None
        new_values_plotdata = None
        if qid_variable_map[qid] in HCS_orderedCats:
            ref_list = HCS_orderedCats[qid_variable_map[qid]]
        if ref_list:
            new_values = list(set(plotdata.index) - set(ref_list))
            if "Other" in ref_list and "Other" in plotdata.index.values:
                new_values = new_values+["Other"]
            new_values_plotdata = plotdata[new_values]
            plotdata = pd.DataFrame(plotdata)
            plotdata["value"] = plotdata.index
            plotdata["value"] = plotdata["value"].apply(lambda row: "Other" if row in new_values else row)
            plotdata = plotdata.groupby("value").sum()[qid].sort_values()
            if qid_variable_map[qid] in HCS_orderedCats:
                plotdata = plotdata.reindex(HCS_orderedCats[qid_variable_map[qid]]).fillna(0.0) #dropna()
        
        plotdata = plotdata.sort_values() if sortByCount else plotdata
        if normalize:
            plotdata = plotdata*100
        
        # do not try to plot empty dataframe
        if len(plotdata)>0:
            #move "Other to bottom"
            label_text = list(plotdata.index.values)
            if "Other" in label_text:
                label_text.remove('Other')
                label_text = ['Other']+label_text
                plotdata = plotdata.reindex(label_text)
            
            plot = plotdata.plot(
                color = hub_color.get(hubname),
                ax = hubaxes.get(hubabbr)[0],
                legend = None,
                kind = kind,                
                title = hubname + " (n = "+str(num_resp)+")", #hubabbr, # individual plot titles
                fontsize = fontsize
                )
            
            axMax = max(axMax,plot.get_xlim()[1])
            
            hubaxes.get(hubabbr)[0].set(xlabel=xlabel,ylabel=ylabel)
            plot.spines['right'].set_visible(False)
            plot.spines['top'].set_visible(False)
            if barlabels:
                fmt = ' %.1f %%' if normalize else ' %.1f'
                plot.bar_label(plot.containers[0], fmt=fmt, label_type='edge',fontsize=fontsize)        
                plot.tick_params(bottom=False, labelbottom=False)
                if orientation=="h":
                    hubaxes.get(hubabbr)[0].set(xlabel="")
                else:
                    hubaxes.get(hubabbr)[0].set(ylabel="")
            valid_plot = True
            
        else:
            print("No data found for Hub = "+hubabbr+" and qid = "+str(qid))
            
        if new_values_plotdata is not None and len(new_values_plotdata)>0:
            if normalize:
                new_values_plotdata = new_values_plotdata/sum(new_values_plotdata)*100
            plot = new_values_plotdata.plot(
                color = hub_color.get(hubname),
                ax = hubaxes.get(hubabbr)[1],
                legend = None,
                kind = kind,                
                title = hubname #hubabbr, # individual plot titles
                )
            hubaxes.get(hubabbr)[1].set(xlabel=xlabel,ylabel=ylabel)
            plot.spines['right'].set_visible(False)
            plot.spines['top'].set_visible(False)
            if barlabels:
                fmt = ' %.1f %%' if normalize else ' %.1f'
                plot.bar_label(plot.containers[0], fmt=fmt, label_type='edge')        
                plot.tick_params(bottom=False, labelbottom=False)
                if orientation=="h":
                    hubaxes.get(hubabbr)[1].set(xlabel="")
                else:
                    hubaxes.get(hubabbr)[1].set(ylabel="")
            valid_plot_other = True      
        

    if valid_plot:
        if title:    
            fig.suptitle(title, size = 20, fontweight = "bold")    
        fig.tight_layout(h_pad=3,w_pad=1.5)
        if equalXaxis:
            for k,v in hubaxes.items():
                v[0].set_xlim((0,axMax)) if orientation == "h" else v[0].set_ylim((0,axMax)) 
       
        if path:
            for ot in outputFormats:
                fig.savefig(path+ot)
            
            
    if valid_plot_other:
        if title:    
            fig_other.suptitle(title+" - Other", size = 20, fontweight = "bold")    
        fig_other.tight_layout(pad=1.5)
            
        if path:
            for ot in outputFormats:
                thisPath = path+ot
                thisPath = thisPath.split(".")
                thisPath[-2] = thisPath[-2]+"_other"
                fig_other.savefig(".".join(thisPath))
    
    plt.close() #close fig
    plt.close() #close fig_other
            
    return {"n":len(data[qid].dropna())}
    
    
    


#####################################################
###### DEFINE FUNCTION TO ITERATE THROUGH HUBS ######
############# MULTIPLE CHOICE QUESTIONS #############
################### COUNT PLOTS #####################
#####################################################

def mcCountPlotSingleHubs(data, mcvar, catByVar = False,
                          figsizex = 20, figsizey = 30, orientation = "h", 
                          barlabels = True, normalize = False, equalXaxis = True,
                          title = "", path = None, outputFormats = [".png"]):

    """Create a figure with six subplots (countplots) according to HMC Hubs.
    Hub plots will be displayed in the official Hub color.
    Optional: Save figure to path.
    
    Args:
        data: pd.DataFrame() 
            The dataframe to be analyzed.
        mcvar: str
            Name of multiple choice variable to be plotted.
        catByVar: bool, default False
            Indicates whether the data is to be categorized by variable. 
            A group plot is generated when the value is True.
        figsizex: int, default 20
            x-value of final figure size.
        figsizey: int, default 30
            y-value of final figure size.
        orientation: ["v", "h"], default "h"
            "v" -> vertical bars
            "h" -> horizontal bars
        barlabels: bool, default True
            When True, labels are added to each bar and x-axis ticks are removed
            When False, x-axis ticks are included and bars have no labels
        normalize: bool, default False
            Indicates whether the counts are to be normalized
        equalXaxis: bool, default True
            Flag indicating whether the x-axis limits for all subplots are to be equal
        title: str, default ""
            Title of Figure.
        path: str, default None
            File path to save figure.
        outputFormats: list, default [".png"]
            list of output formats in which the plot is to be exported.
        
    Returns:
        dict {"n":<length of plotted data>}
    """
    
    #find list of questions for the given mcvar
    qid_list = [key for key,val in qid_variable_map.items() if mcvar in val]
    
    #drop rows where all qids in qid _list are empty
    data = data.dropna(subset=qid_list, how='all')
    
    # assign cell values as variable name instead of True
    other = None
    values = []
    for qid in qid_list:
        if "other" not in qid:
            values.append(qid)
    for col in data.columns:
        if col in values and mc_subquestions.get(mcvar):
            data[col] = data[col].replace({True:mc_subquestions.get(mcvar).get(qid_variable_map.get(col))})
    
    #get the question id of the question on hubs
    qid_hub = [key for key,val in qid_variable_map.items() if val == "researchFieldHGF"][0]
    
    newdf = pd.melt(frame=data,id_vars=qid_hub, value_vars=qid_list, value_name=mcvar)
    newdf = newdf.dropna(subset=[mcvar])
    if mcvar in mc_var_group_plot:
        newdf = newdf.replace({"variable":qid_variable_map}).replace({"variable":mc_subquestions[mcvar]})
    newdf[mcvar] = newdf[mcvar].astype("category")
    
    if orientation == "v":
        kind = "bar"
        xlabel = ""
        ylabel = "percentage" if normalize else "count"
    elif orientation == "h":
        kind = "barh"
        xlabel = "percentage" if normalize else "count"
        ylabel = ""
    else:
        print("orientation = "+str(orientation)+"\nUnknown orientation value provided. Orientation needs to be either \"v\" or \"h\"")
        
    fig = plt.figure(figsize = (figsizex,figsizey))

    ax1 = fig.add_subplot(321)
    ax2 = fig.add_subplot(322)
    ax3 = fig.add_subplot(323)
    ax4 = fig.add_subplot(324)
    ax5 = fig.add_subplot(325)
    ax6 = fig.add_subplot(326)

    fig_other = plt.figure(figsize = (figsizex,figsizey))
    
    ax1_other = fig_other.add_subplot(321)
    ax2_other = fig_other.add_subplot(322)
    ax3_other = fig_other.add_subplot(323)
    ax4_other = fig_other.add_subplot(324)
    ax5_other = fig_other.add_subplot(325)
    ax6_other = fig_other.add_subplot(326)
    
    #axes are defined in the same order as answer options
    hubaxes = {
        'AST':[ax5,ax5_other],
        'E&E':[ax2,ax2_other],
        'Energy':[ax1,ax1_other],
        'Health':[ax3,ax3_other],
        'Info':[ax4,ax4_other],
        'Matter':[ax6,ax6_other]
    }
    
    #get the question id of the question on hubs
    qid_hub = [key for key,val in qid_variable_map.items() if val == "researchFieldHGF"][0]
    
    valid_plot = False
    valid_plot_other = False
    
    axMax = 0
    
    for hubname,hubabbr in hubdict.items():
        plotdata_ = newdf.loc[newdf[qid_hub] == hubname]
        num_resp = len(data[data[qid_hub]==hubname])
        
        # do not try to plot empty dataframe
        if len(plotdata_)>0:
            if catByVar:
                plotdata = plotdata_.pivot_table(index="variable", columns=mcvar, 
                                            values='variable', aggfunc='size')
                # separate newly defined categories and add count to "other"
                ref_list = None
                new_values_plotdata = None
                if mcvar in mc_subquestions:
                    ref_list = mc_subquestions[mcvar].values()
                if ref_list:
                    new_values = list(set(plotdata.index) - set(ref_list))
                    if "Other" in ref_list:
                        new_values = new_values+["Other"]
                    plotdata = plotdata.reset_index()
                    new_values_plotdata = plotdata[plotdata["variable"].isin(new_values)]
                    plotdata["variable"] = plotdata["variable"].apply(lambda row: "Other" if row in new_values else row)
                    plotdata = plotdata.groupby("variable").sum()
                    if mcvar in HCS_orderedCats:
                        plotdata = plotdata.reindex(
                            index = [v for v in HCS_orderedCats[mcvar]["rows"]] if HCS_orderedCats[mcvar]["rows"] else None,
                            columns = [v for v in HCS_orderedCats[mcvar]["columns"]])
                
                if normalize:
                    plotdata = plotdata.div(len(data[data[qid_hub]==hubname])).mul(100)
                
                bbox_to_anchor = (0.5,-0.25) if barlabels else (0.5,-0.15)
                
                label_text = list(plotdata.index.values)
                if "Other" in label_text:
                    label_text.remove('Other')
                    label_text = label_text+['Other']
                    plotdata = plotdata.reindex(label_text)
                
                plot = plotdata.plot(
                    ax = hubaxes.get(hubabbr)[0],
                    legend = None,
                    kind = kind,
                    ylabel = "",
                    color=['#648FFF','#FFB000','#D8006A'],
                    title = hubname + " (n = "+str(num_resp)+")" #hubabbr # individual plot titles
                    )
                
                axMax = max(axMax,plot.get_xlim()[1])
                
                hubaxes.get(hubabbr)[0].invert_yaxis()
                hubaxes.get(hubabbr)[0].set(xlabel=xlabel,ylabel=ylabel)
                plot.spines['right'].set_visible(False)
                plot.spines['top'].set_visible(False)
                if barlabels:
                    fmt = ' %.1f %%' if normalize else ' %.1f'
                    for con in plot.containers:
                        plt.bar_label(con, fmt=fmt, label_type='edge')
                    plot.tick_params(bottom=False, labelbottom=False)
                    if orientation=="h":
                        hubaxes.get(hubabbr)[0].set(xlabel="")
                    else:
                        hubaxes.get(hubabbr)[0].set(ylabel="")
                valid_plot = True
                
                if new_values_plotdata is not None and len(new_values_plotdata)>0:
                    if normalize:
                        for ci in new_values_plotdata.columns[1:]:
                            new_values_plotdata[ci] = new_values_plotdata[ci]/sum(new_values_plotdata[ci])*100
                    new_values_plotdata = new_values_plotdata.groupby("variable").sum()
                    plot = new_values_plotdata.plot(
                        ax = hubaxes.get(hubabbr)[1],
                        legend = None,
                        kind = kind,
                        ylabel = "",
                        color=['#648FFF','#FFB000','#D8006A'],
                        title = hubname + " - Other" #hubabbr # individual plot titles
                        )
                    hubaxes.get(hubabbr)[1].invert_yaxis()
                    hubaxes.get(hubabbr)[1].set(xlabel=xlabel,ylabel=ylabel)
                    plot.spines['right'].set_visible(False)
                    plot.spines['top'].set_visible(False)
                    if barlabels:
                        fmt = ' %.1f %%' if normalize else ' %.1f'
                        for con in plot.containers:
                            plt.bar_label(con, fmt=fmt, label_type='edge')
                        plot.tick_params(bottom=False, labelbottom=False)
                        if orientation=="h":
                            hubaxes.get(hubabbr)[1].set(xlabel="")
                        else:
                            hubaxes.get(hubabbr)[1].set(ylabel="")
                    valid_plot_other = True
           
            else:
                plotdata = plotdata_.loc[newdf[qid_hub] == hubname][mcvar].value_counts().sort_index()
                
                ref_list = None
                new_values_plotdata = None
                if mcvar in mc_subquestions:
                    ref_list = mc_subquestions[mcvar].values()
                if ref_list:
                    new_values = list(set(plotdata.index) - set(ref_list))
                    if "Other" in ref_list:
                        new_values = new_values+["Other"]
                    new_values_plotdata = plotdata[new_values]
                    plotdata = pd.DataFrame(plotdata)
                    plotdata["value"] = plotdata.index
                    plotdata["value"] = plotdata["value"].apply(lambda row: "Other" if row in new_values else row)
                    plotdata = plotdata.groupby("value").sum()[mcvar].sort_values()
                    if mcvar in HCS_orderedCats:
                        plotdata = plotdata.reindex(HCS_orderedCats[mcvar]).dropna()
                
                if normalize:
                    plotdata = plotdata/num_resp*100
                    
                label_text = list(plotdata.index.values)
                if "Other" in label_text:
                    label_text.remove('Other')
                    label_text = ['Other']+label_text
                    plotdata = plotdata.reindex(label_text)
                    
                plot = plotdata.plot(
                    color = hub_color.get(hubname),
                    ax = hubaxes.get(hubabbr)[0],
                    legend = None,
                    kind = kind,
                    title = hubname + " (n = "+str(num_resp)+")" #hubabbr # individual plot titles
                    )
                
                axMax = max(axMax,plot.get_xlim()[1])
                
                #set axes labels
                hubaxes.get(hubabbr)[0].set(xlabel=xlabel,ylabel=ylabel)
                plot.spines['right'].set_visible(False)
                plot.spines['top'].set_visible(False)
                if barlabels:
                    fmt = ' %.1f %%' if normalize else ' %.1f'
                    plot.bar_label(plot.containers[0], fmt=fmt, label_type='edge')        
                    plot.tick_params(bottom=False, labelbottom=False)
                    if orientation=="h":
                        hubaxes.get(hubabbr)[0].set(xlabel="")
                    else:
                        hubaxes.get(hubabbr)[0].set(ylabel="")
                valid_plot = True
                
                if new_values_plotdata is not None and len(new_values_plotdata)>0:
                    if normalize:
                        new_values_plotdata = new_values_plotdata/sum(new_values_plotdata)*100
                    plot = new_values_plotdata.plot(
                        color = hub_color.get(hubname),
                        ax = hubaxes.get(hubabbr)[1],
                        legend = None,
                        kind = kind,
                        title = hubname #hubabbr + " - Other" # individual plot titles
                    )
                    hubaxes.get(hubabbr)[1].set(xlabel=xlabel,ylabel=ylabel)
                    plot.spines['right'].set_visible(False)
                    plot.spines['top'].set_visible(False)
                    if barlabels:
                        fmt = ' %.1f %%' if normalize else ' %.1f'
                        plot.bar_label(plot.containers[0], fmt=fmt, label_type='edge')        
                        plot.tick_params(bottom=False, labelbottom=False)
                        if orientation=="h":
                            hubaxes.get(hubabbr)[1].set(xlabel="")
                        else:
                            hubaxes.get(hubabbr)[1].set(ylabel="")
                    valid_plot_other = True
        else:
            print("No data found for Hub = "+hubabbr+" and variable = "+str(mcvar))
        
    if valid_plot:
        if title:    
            fig.suptitle(title, size = 20, fontweight = "bold")    
        fig.tight_layout(h_pad=3,w_pad=1.5)
        
        if equalXaxis:
            for k,v in hubaxes.items():
                v[0].set_xlim((0,axMax)) if orientation == "h" else v[0].set_ylim((0,axMax)) 
           
        if catByVar:
            fig.legend(loc="lower center", bbox_to_anchor=bbox_to_anchor,
                                labels=new_values_plotdata.columns)                  
            
        if path:
            for ot in outputFormats:
                fig.savefig(path+ot)
            
            
    if valid_plot_other:
        if title:    
            fig_other.suptitle(title+" - Other", size = 20, fontweight = "bold")    
        fig_other.tight_layout(pad=1.5)
        
        if catByVar:
            fig_other.legend(loc="lower center", bbox_to_anchor=bbox_to_anchor,
                                labels=new_values_plotdata.columns)                   
            
        if path:
            for ot in outputFormats:
                thisPath =  path+ot
                thisPath = thisPath.split(".")
                thisPath[-2] = thisPath[-2]+"_other"
                fig_other.savefig(".".join(thisPath))
        
    plt.close() #close fig
    plt.close() #close fig_other
            
    return {"n":len(data)}
        


###############################
####### EDA ALL ANSWERS #######
###### SORTED COUNTPLOTS ######
######## SINGLE CHOICE ########
###############################

def scEDAcountPlot(data, qid, sortByCount = True, orientation = "h",
                   figsizex = 10, figsizey = 10,
                   barlabels = True, normalize = False, 
                   title = "", path = None, outputFormats = [".png"]):

    """Create a figure (countplot). Bars are displayed in default HMC color.
    Optional: Save figure to path.
    
    Args:
        data: pd.DataFrame() 
            The dataframe to be analyzed.
        qid: str
            Name of column in the dataframe to be plotted.
        sortByCount: bool, default True
            Indicates whether the data is to be sorted by count.
        orientation: ["v", "h"], default "h"
            "v" -> vertical bars
            "h" -> horizontal bars
        figsizex: int, default 10
            x-value of final figure size.
        figsizey: int, default 10
            y-value of final figure size.
        barlabels: bool, default True
            When True, labels are added to each bar and x-axis ticks are removed
            When False, x-axis ticks are included and bars have no labels
        normalize: bool, default False
            Indicates whether the counts are to be normalized
        title: str, default ""
            Title of Figure.
        path: str, default None
            File path to save figure.
        outputFormats: list, default [".png"]
            list of output formats in which the plot is to be exported.
        
    Returns:
        dict {"n":<length of plotted data>}
    """
    if orientation == "v":
        kind = "bar"
        xlabel = ""
        ylabel = "percentage" if normalize else "count"
    elif orientation == "h":
        kind = "barh"
        xlabel = "percentage" if normalize else "count"
        ylabel = ""
    else:
        print("orientation = "+str(orientation)+"\nUnknown orientation value provided. Orientation needs to be either \"v\" or \"h\"")
    
    plotdata = data[qid].value_counts(normalize=normalize).sort_values()
    # separate newly defined categories and add count to "other"
    ref_list = None
    new_values_plotdata = None
    if qid_variable_map[qid] in HCS_orderedCats:
        ref_list = HCS_orderedCats[qid_variable_map[qid]]
    if ref_list:
        new_values = list(set(plotdata.index) - set(ref_list))
        if "Other" in ref_list:
                new_values = new_values+["Other"]
        new_values_plotdata = plotdata[new_values]
        plotdata = pd.DataFrame(plotdata)
        plotdata["value"] = plotdata.index
        plotdata["value"] = plotdata["value"].apply(lambda row: "Other" if row in new_values else row)
        plotdata = plotdata.groupby("value").sum()[qid].sort_values()
        if qid_variable_map[qid] in HCS_orderedCats:
           plotdata = plotdata.reindex(HCS_orderedCats[qid_variable_map[qid]]).dropna()
    
    if normalize:
        plotdata = plotdata*100 
    plotdata = plotdata.sort_index() if not sortByCount else plotdata
    
    label_text = list(plotdata.index.values)
    if "Other" in label_text:
        label_text.remove('Other')
        label_text = ['Other']+label_text
        plotdata = plotdata.reindex(label_text)
            
    plot = plotdata.plot(
        color = "#005AA0",
        legend = None,
        kind = kind,
        title = title,
        figsize=(figsizex,figsizey)
        )
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plot.spines['right'].set_visible(False)
    plot.spines['top'].set_visible(False)
    if barlabels:
        fmt = ' %.1f %%' if normalize else ' %.1f'
        plt.bar_label(plot.containers[0], fmt=fmt, label_type='edge')
        if orientation=="h":
            plot.spines['bottom'].set_visible(False)
            plot.tick_params(bottom=False, labelbottom=False)
            plt.xlabel("")
        else:
            plot.spines['left'].set_visible(False)
            plot.tick_params(left=False, labelleft=False)
            plt.ylabel("")
    plt.tight_layout(pad=1.5)    
    
    for ot in outputFormats:
        plt.savefig(path+ot)
    plt.close()
    
    if new_values_plotdata is not None and len(new_values_plotdata)>0:
        if normalize:
            new_values_plotdata = new_values_plotdata/sum(new_values_plotdata)*100
        
        plot = new_values_plotdata.plot(
            color = "#005AA0",
            legend = None,
            kind = "barh",
            title = title+" - Other"
            )
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plot.spines['right'].set_visible(False)
        plot.spines['top'].set_visible(False)
        if barlabels:
            fmt = ' %.1f %%' if normalize else ' %.1f'
            plt.bar_label(plot.containers[0], fmt=fmt, label_type='edge')
            plot.spines['bottom'].set_visible(False)
            plot.tick_params(bottom=False, labelbottom=False)
            plt.xlabel("")
        plt.tight_layout(pad=1.5)
        
        for ot in outputFormats:
            thisPath = path+ot
            thisPath = thisPath.split(".")
            thisPath[-2] = thisPath[-2]+"_other"
            plt.savefig(".".join(thisPath))
        plt.close()

    return {"n":len(data[qid].dropna())}

###############################
####### EDA ALL ANSWERS #######
###### SORTED COUNTPLOTS ######
####### MULTIPLE CHOICE #######
###############################

def mcEDAcountPlot(data, mcvar, catByVar = False, 
                   figsizex = 10, figsizey = 10,
                   barlabels = True, normalize = False, 
                   title = "", path = None, outputFormats = [".png"]):
    
    """Create a figure (countplot). Bars are displayed in default HMC color.
    Optional: Save figure to path.
    
    Args:
        data: pd.DataFrame() 
            The dataframe to be analyzed.
        mcvar: str
            Name of multiple choice variable to be plotted.
        catByVar: bool, default False
            Indicates whether the data is to be categorized by variable. 
            A group plot is generated when the value is True.
        figsizex: int, default 10
            x-value of final figure size.
        figsizey: int, default 10
            y-value of final figure size.
        barlabels: bool, default True
            When True, labels are added to each bar and x-axis ticks are removed
            When False, x-axis ticks are included and bars have no labels
        normalize: bool, default False
            Indicates whether the counts are to be normalized
        title: str, default ""
            Title of Figure.
        path: str, default None
            File path to save figure.
        outputFormats: list, default [".png"]
                list of output formats in which the plot is to be exported.
        
    Returns:
        dict {"n":<length of plotted data>}
    """
    
    xlabel = "percentage" if normalize else "count"
    ylabel = ""

    #find list of questions for the given mcvar
    qid_list = [key for key,val in qid_variable_map.items() if mcvar in val]
    
    #drop rows where all qids in qid _list are empty
    data = data.dropna(subset=qid_list, how='all')
   
    # assign cell values as variable name instead of True
    other = None
    values = []
    for qid in qid_list:
        if "other" not in qid:
            values.append(qid)
    for col in data.columns:
        if col in values and mc_subquestions.get(mcvar):
            data[col] = data[col].replace({True:mc_subquestions.get(mcvar).get(qid_variable_map.get(col))})


    #get the question id of the question on hubs
    qid_hub = [key for key,val in qid_variable_map.items() if val == "researchFieldHGF"][0]
    
    newdf = pd.melt(frame=data,id_vars=qid_hub, value_vars=qid_list, value_name=mcvar)
    newdf = newdf.dropna(subset=[mcvar])
    if mcvar in mc_var_group_plot:
        newdf = newdf.replace({"variable":qid_variable_map}).replace({"variable":mc_subquestions[mcvar]})
    newdf[mcvar] = newdf[mcvar].astype("category")
    
    if catByVar:
        plotdata = newdf.groupby(['variable',mcvar]).count().unstack()
        # separate newly defined categories and add count to "other"
        ref_list = None
        new_values_plotdata = None
        if mcvar in mc_subquestions:
            ref_list = mc_subquestions[mcvar].values()
        if ref_list:
            new_values = list(set(plotdata.index) - set(ref_list))
            if "Other" in ref_list:
                new_values = new_values+["Other"]
                
            plotdata = plotdata.reset_index(level=[0])
            new_values_plotdata = plotdata[plotdata["variable"].isin(new_values)]
            plotdata["variable"] = plotdata["variable"].apply(lambda row: "Other" if row in new_values else row)
            plotdata = plotdata.groupby("variable").sum()
            if mcvar in HCS_orderedCats:
                plotdata = plotdata.reindex(
                    index = [v for v in HCS_orderedCats[mcvar]["rows"]] if HCS_orderedCats[mcvar]["rows"] else None,
                    columns = [tuple([qid_hub,v]) for v in HCS_orderedCats[mcvar]["columns"]])
                #plotdata = plotdata.sort_values((qid_hub,HCS_orderedCats[mcvar]["columns"][0]),ascending=False)
        
        # add number of respondants for each answer option
        plotdata["n"] = plotdata[qid_hub].sum(axis=1)
        
        if normalize:
            plotdata[qid_hub] = plotdata[qid_hub].div(plotdata["n"],axis="index")*100
        
        bbox_to_anchor = (0.5,-0.25) if barlabels else (0.5,-0.15)
        
        plot = plotdata[qid_hub].plot(
            kind = "barh",
            title = title,
            width = 0.7,
            figsize=(figsizex,figsizey),
            color=['#648FFF','#FFB000','#D8006A']
            )
        plt.gca().invert_yaxis()
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plot.legend(loc="lower center", bbox_to_anchor=bbox_to_anchor,
                    labels=[x[1] for x in plotdata.columns])
        plot.spines['right'].set_visible(False)
        plot.spines['top'].set_visible(False)
        
        #add n to labels
        label_text = plot.get_yticklabels() #if orientation == "h" else plot.get_xticklabels()         
        for item in label_text:
            item.set_text(item.get_text()+"\n(n = "+str(int(plotdata.loc[item.get_text()]["n"]))+")")
        plot.set_yticklabels(label_text) #if orientation == "h" else plot.set_xticklabels(label_text)
        if barlabels:
            fmt = ' %.1f %%' if normalize else ' %.1f'
            for con in plot.containers:
                plt.bar_label(con, fmt=fmt, label_type='edge',fontsize=12)
            plot.spines['bottom'].set_visible(False)
            plot.tick_params(bottom=False, labelbottom=False)
            plt.xlabel("")
        plt.tight_layout(pad=1.5)        
        
        for ot in outputFormats:
            plt.savefig(path+ot)
        plt.close()
        
        if new_values_plotdata is not None and len(new_values_plotdata)>0:
            if normalize:
                for ci in new_values_plotdata.columns[1:]:
                    new_values_plotdata[ci] = new_values_plotdata[ci]/sum(new_values_plotdata[ci])*100
            new_values_plotdata = new_values_plotdata.groupby("variable").sum()
            plot = new_values_plotdata.plot(
                legend = None,
                kind = "barh",
                title = title+" - Other",
                color=['#648FFF','#FFB000','#D8006A']
                )
            plt.gca().invert_yaxis()
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plot.legend(loc="lower center", bbox_to_anchor=bbox_to_anchor,
                    labels=[x[1] for x in new_values_plotdata.columns])
            plot.spines['right'].set_visible(False)
            plot.spines['top'].set_visible(False)
            if barlabels:
                fmt = ' %.1f %%' if normalize else ' %.1f'
                for con in plot.containers:
                    plt.bar_label(con, fmt=fmt, label_type='edge')
                plot.spines['bottom'].set_visible(False)
                plot.tick_params(bottom=False, labelbottom=False)
                plt.xlabel("")
            plt.tight_layout(pad=1.5)
            
            for ot in outputFormats:
                thisPath = path+ot
                thisPath = thisPath.split(".")
                thisPath[-2] = thisPath[-2]+"_other"
                plt.savefig(".".join(thisPath))
            plt.close()
        
    else:
        plotdata = newdf[mcvar].value_counts().sort_values()
        # separate newly defined categories and add count to "other"
        ref_list = None
        new_values_plotdata = None
        if mcvar in mc_subquestions:
            ref_list = mc_subquestions[mcvar].values()
        if ref_list:
            new_values = list(set(plotdata.index) - set(ref_list))
            if "Other" in ref_list:
                new_values = new_values+["Other"]
            new_values_plotdata = plotdata[new_values]
            plotdata = pd.DataFrame(plotdata)
            plotdata["value"] = plotdata.index
            plotdata["value"] = plotdata["value"].apply(lambda row: "Other" if row in new_values else row)
            plotdata = plotdata.groupby("value").sum()[mcvar].sort_values()
            if mcvar in HCS_orderedCats:
                plotdata = plotdata.reindex(HCS_orderedCats[mcvar]).dropna()
            
        if normalize:
            plotdata = plotdata/len(data)*100
            
        label_text = list(plotdata.index.values)
        if "Other" in label_text:
            label_text.remove('Other')
            label_text = ['Other']+label_text
            plotdata = plotdata.reindex(label_text)
        
        plot = plotdata.plot(
            color = "#005AA0",
            kind = "barh",
            title = title,
            figsize=(figsizex,figsizey)
            )
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plot.spines['right'].set_visible(False)
        plot.spines['top'].set_visible(False)
        if barlabels:
            fmt = ' %.1f %%' if normalize else ' %.1f'
            plt.bar_label(plot.containers[0], fmt=fmt, label_type='edge')
            plot.spines['bottom'].set_visible(False)
            plot.tick_params(bottom=False, labelbottom=False)
            plt.xlabel("")
        plt.tight_layout(pad=1.5)
        
        for ot in outputFormats:
            plt.savefig(path+ot)
        plt.close()
    
        if new_values_plotdata is not None and len(new_values_plotdata)>0:
            if normalize:
                new_values_plotdata = new_values_plotdata/sum(new_values_plotdata)*100
            plot = new_values_plotdata.plot(
                color = "#005AA0",
                legend = None,
                kind = "barh",
                title = title+" - Other",
                )
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plot.spines['right'].set_visible(False)
            plot.spines['top'].set_visible(False)
            if barlabels:
                fmt = ' %.1f %%' if normalize else ' %.1f'
                plt.bar_label(plot.containers[0], fmt=fmt, label_type='edge')
                plot.spines['bottom'].set_visible(False)
                plot.tick_params(bottom=False, labelbottom=False)
                plt.xlabel("")
            plt.tight_layout(pad=1.5)
            
            for ot in outputFormats:
                thisPath = path+ot
                thisPath = thisPath.split(".")
                thisPath[-2] = thisPath[-2]+"_other"
                plt.savefig(".".join(thisPath))
            plt.close()

    return {"n":len(data)}

###############################
####### EDA ALL ANSWERS #######
######### UPSET PLOTS #########
####### MULTIPLE CHOICE #######
###############################

def mcUpSetPlot(data, mcvar, catByVar = False, 
                   figsizex = 10, figsizey = 10,
                   barlabels = True, normalize = False, 
                   orientation = "vertical",
                   title = "", path = None, outputFormats = [".png"]):
    
    """Create a figure (UpSet plot).
    Optional: Save figure to path.
    
    Args:
        data: pd.DataFrame() 
            The dataframe to be analyzed.
        mcvar: str
            Name of multiple choice variable to be plotted.
        catByVar: bool, default False
            Indicates whether the data is to be categorized by variable. 
            A group plot is generated when the value is True.
        figsizex: int, default 10
            x-value of final figure size.
        figsizey: int, default 10
            y-value of final figure size.
        barlabels: bool, default True
            When True, labels are added to each bar and x-axis ticks are removed
            When False, x-axis ticks are included and bars have no labels
        normalize: bool, default False
            Indicates whether the counts are to be normalized
        orientation: ["vertical", "horizontal"], default "horizontal"
            Orientation of the UpSet plot
        title: str, default ""
            Title of Figure.
        path: str, default None
            File path to save figure.
        outputFormats: list, default [".png"]
                list of output formats in which the plot is to be exported.
        
    Returns:
        dict {"n":<length of plotted data>}
    """
    
    ylabel = "Percentage" if normalize else "count"
    xlabel = ""
    
    barlabels = "%.1f" if barlabels else False

    #find list of questions for the given mcvar
    qid_list = [key for key,val in qid_variable_map.items() if mcvar in val]
    
    #drop rows where all qids in qid _list are empty
    data = data[qid_list].dropna(how='all')
   
    #replace all Nan by False and values by True
    for qid in qid_list:
        data[qid] = data[qid].apply(lambda x: False if pd.isna(x) else True)
        
    #replace column qid by answer option
    colnames = data.columns
    new_colnames = []
    for col in colnames:
        if mc_subquestions.get(mcvar):
            if "other" in col:
                new_colnames.append("Other")
            else:
                new_colnames.append(mc_subquestions.get(mcvar).get(qid_variable_map.get(col)))
    data.columns = new_colnames
    
    if catByVar:        
        print("Not implemented!")
        
    else:
        # convert df to multi indexed series
        plotdata = data.groupby(new_colnames).size()
         
        if normalize:
            plotdata = plotdata/len(data)*100
            
        plot = up.plot(plotdata,
                       facecolor = "#005AA0", 
                       orientation = orientation,
                       sort_by = "cardinality",
                       show_counts = barlabels)
                
        plt.xlabel("xlabel")
        plt.ylabel(ylabel)
        plt.suptitle(title)
                     
        for ot in outputFormats:
            plt.savefig(path+ot)
        plt.close()
        
    return {"n":len(data)}


def scPieChart(data, qid, normalize = True,
               figsizex = 10, figsizey = 10, 
               title = "", path = None, outputFormats = [".png"]):
    
    """Create a pie chart. Pies are colored in HMC Hub colors.
    Optional: Save figure to path.
    
    Args:
        data: pd.DataFrame() 
            The dataframe to be analyzed.
        qid: str
            Name of column in the dataframe to be plotted.
        normalize: bool, default False
            Indicates whether the counts are to be normalized            
        figsizex: int, default 10
            x-value of final figure size.
        figsizey: int, default 10
            y-value of final figure size.
        title: str, default ""
            Title of Figure.
        path: str, default None
            File path to save figure.
        outputFormats: list, default [".png"]
                list of output formats in which the plot is to be exported.
        
    Returns:
        dict {"n":<length of plotted data>}
    """
    
    plotdata = data[qid].value_counts()
    
    # create color list in the right order
    colors = []    
    for h in plotdata.index:
        colors.append([c for k,c in hub_color.items() if h==k][0])
    
    # define plot labels
    plotlabels = []
    for i in range(len(plotdata.index)):
        value = str(plotdata[plotdata.index[i]])
        if normalize:
            value = round(plotdata[plotdata.index[i]]/sum(plotdata)*100)
            value = str(value)+"%"            
        plotlabels.append(plotdata.index[i]+" ("+value+")")
    
    fig, ax = plt.subplots(figsize=(figsizex,figsizey))
    
    ### PLOT ###
    plot = ax.pie(
            plotdata,
            labels = plotlabels,
            colors = colors,
            explode = [0.02]*len(plotdata),
            radius = 0.8,
            textprops={'fontsize': 14})
    fig.tight_layout(pad=1.5)
    
    if title: 
        fig.suptitle(title, size = 20, fontweight = "bold")

    for ot in outputFormats:
        fig.savefig(path+ot)
    plt.close()

    return {"n":len(data[qid].dropna())}
    

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval

#dummy function, so that hifis doesn't throw an error
def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    pass